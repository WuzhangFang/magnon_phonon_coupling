import numpy as np
import matplotlib.pyplot as plt
from scipy import linalg

def disp(h,k,branch,param,S):
    """ 
    Returns magnon dispersion of honeycomb lattice 
    at the (h,k) point in reciprocal space. 
    Arguments:
        h: real, coordinate along the B1 generator.
        k: real, coordinate along the B2 generator.
        branch: integer, 0 lower branch, 1 upper branch
        param: real, [J1, J2, J3, A2, Dz], with
            J1: nearest-neighbour isotropic exchange (in meV). 
            J2: next-nearest-neighbour isotropic exchange (in meV). 
            J3: next-next-nearest-neighbour isotropic exchange (in meV). 
            A2: next-nearest-neighbour Dzyaloshinskii-Moriya interaction (in meV). 
            Dz: single-ion anisotropy (in meV). 
        S: real, Sz expectation value in hbar units. 
    Returns:
        Magnon energy of the selected branch in meV.
    """

    J1 = param[0]
    J2 = param[1]
    J3 = param[2]
    A2 = param[3]
    Dz = param[4]

    c1 = np.cos(h*2.*np.pi)
    c2 = np.cos(k*2.*np.pi)
    c3 = np.cos((h+k)*2.*np.pi)

    s1 = np.sin(h*2.*np.pi)
    s2 = np.sin(k*2.*np.pi)
    s3 = np.sin((h+k)*2.*np.pi)

    gq = 2.*(c1+c2+c3)
    fq = 2.*(s1+s2-s3)
    hq = 8.*c1*c2*c3+1
    lq = 4.*(c1*c2 + c1*c3 + c2*c3)

    sq = np.sqrt( (A2*fq)**2 + (3+gq)*J1**2 + (gq+lq)*J1*J3 + hq*J3**2 )

    # eq. (7) in 10.1103/PhysRevB.107.214452
    I = 3*J1 + 6*J2 + 3*J3 + 2*Dz 

    # more details in eq. (8) in 10.1103/PhysRevB.106.214424
    if branch == 0:
        res = S*(I - J2*gq - sq)
    else:
        res = S*(I - J2*gq + sq)
    return res


#PRX 8, 041028 parameters
#
J1 =  2.01
J2 =  0.16
J3 = -0.08
#A2 =  0.31
A2 =  0.0
Dz = 0.22

param_exp = np.array([J1,J2,J3,A2,Dz])

# DFT parameters 
#
J1 =  2.756
J2 =  0.815
J3 = -0.271
A2 = 0
Dz = 0.68/(9./4)

param_dft = np.array([J1,J2,J3,A2,Dz])

Sz = 1.5

# number of q points
nq = 1000
w1 = np.zeros(nq)
w2 = np.zeros(nq)
qs = np.linspace(0, 2, num=nq)


for iq, q in enumerate(qs):
    q = q / 2.0
    w1[iq] = disp(q, q, 0, param_exp, Sz)
    w2[iq] = disp(q, q, 1, param_exp, Sz)

plt.plot(qs, w1)
plt.plot(qs, w2)
plt.show()
