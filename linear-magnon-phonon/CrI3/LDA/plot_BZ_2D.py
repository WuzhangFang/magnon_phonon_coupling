import numpy as np
import matplotlib.pyplot as plt
from helper import get_reciprocal
np.set_printoptions(precision=6)

# plot the lambada on a grid in BZ
# grid interpolation
system = 'q31x31'
label = 'DFT_LDA'
lambda_q_27 = np.load(f'lambda_{system}_{label}.npy')
print('shape of lambda_q: ', lambda_q_27.shape)
phonon_folder = '../data_phonon'
q_27 = np.load(f'{phonon_folder}/q_points_{system}.npy')
print('shape of q_points: ', q_27.shape)

# reciprocal lattice in Cartesian coordinates
a = 7.0017278277323989
a1 = np.array([a, 0.0, 0.0])
a2 = np.array([-a / 2.0, np.sqrt(3) * a / 2.0, 0.0])
a3 = np.array([0.0, 0.0, 23.0])
b1, b2, b3 = get_reciprocal([a1, a2, a3])

# magnon and phonon mode
i_mag = 0
i_ph = 15
# coupling
cvalues = np.abs(lambda_q_27[:, i_mag, i_ph])

fig, ax = plt.subplots()
fs=12

# plot q grid
for iq in range(q_27.shape[0]):
    q1, q2, q3 = q_27[iq]
    q = q1 * b1 + q2 * b2 + q3 * b3
    a = ax.scatter(q[0], q[1], c=cvalues[iq], cmap='Blues', s=125, alpha=1.0, 
                        edgecolors='none', vmin=0.0, vmax=max(cvalues))

# Add colorbar
cbar = fig.colorbar(a, ax=ax)
cbar.set_label('|$\lambda$| (mev)', rotation=270, labelpad=25, fontsize=fs)

plt.show()