# Magnon_phonon_coupling in CrI3

## Phonopy calculations

The eigenvalues and eigenvectors on different kpath/grid are calculated using phonopy in `phonopy` folder.

## Phonon calculations

The eigenvalues and eigenvectors of phonons on different kpath/grid are stored in `data_phonon`.

## Magnon calculations

The eigenvalues and eigenvectors of magnons are stored in `data_magnon` with different suffix for different parameters. Run `python get_magnon.py` first. We load the qpoints in phonon calculations. Magnon eigenvalues and eigenvectors are determined by exchange constants. Depending on the approximation, the exchange constants would be different. We can also use the exchange constants fitted by neutron scattering experiments.

- `_q51x51_DFT`: on a 51x51 q-grid with calculated exchange constants by DFT.

- `_GKM_exp`: along kpath GKM with experimental exchange constants.

- `_q3x3_DFT`: on a 3x3 q-grid with calculated exchange constants by DFT.

## Coupling constant

The derivatives of off-diagonal exchange constants are calculated in `supercell_3x3` and `supercell_6x6`. The coupling constants are calculated and stored in these folders with different suffix.

- Run `python get_coupling.py` first to generate and store the coupling constants. In the begining, we need to change the data folders for magnons and phonons.

- Run `python plot_BZ.py` to plot the coupling constants on a grid in BZ.

- Run `python plot_GKM_exp.py` to plot the quasi-particle bandstructure along kpath GKM with experimental exchange constants.

- Run `python plot_GKM_constant.py` to plot the quasi-particle bandstructure along kpath GKM with a constant $\lambda=0.2$ meV.

## Scattering rate

- Run `python get_lifetime.py` to get the scattering rate for acoustic and optic magnon modes.