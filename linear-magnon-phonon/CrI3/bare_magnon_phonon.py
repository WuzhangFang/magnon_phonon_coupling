import numpy as np
import matplotlib.pyplot as plt
from helper import model

# CrI3
# lattice vectors
a1 = np.array([1.0, 0.0, 0.0])
a2 = np.array([-1/2.0, np.sqrt(3)/2.0, 0.0])
a3 = np.array([0.0, 0.0, 1.0])

S = 3/2
# unit: meV parameters from
# https://journals.aps.org/prb/abstract/10.1103/PhysRevB.106.214424
# notice there is 1/2 as a prefactor in spin Hamiltonian
J1, J2, J3 = (2.94, 0.62, -0.16)
z1, z2, z3 = (3, 6, 3)

cri3 = model([a1, a2, a3], [J1, J2, J3], [z1, z2, z3], S)
# reciprocal vectors
b1, b2, b3 = cri3.get_reciprocal()

# plot band structure
G = 0.0 * b1 + 0.0 * b2
K = 0.333333 * b1 + 0.333333 * b2
M = 0.5 * b1 + 0.0 * b2
kpoints = np.array([G, K, M, G])

def H_k(k, J_list, z_list, S):
    """
    for magnon
    up to third-nearest neighbor Hamiltonian
    for each k=[kx, ky], return the eigenvalue
    the basis set is a+, b+ (row) a, b (column)
    """
    # number of bands
    nb = 2
    kx = k[0]
    ky = k[1]
    H_k = np.zeros((nb, nb), dtype=complex)
    J1, J2, J3 = J_list
    z1, z2, z3 = z_list
    Ak = z1 * J1 + z2 * J2 + z3 * J3 - 2 * J2 * (np.cos(kx) + 2*np.cos(kx/2)*np.cos(np.sqrt(3)*ky/2))
    Bk = -J1 * (np.exp(1j*np.sqrt(3)*ky/3) + 2*np.cos(kx/2)*np.exp(-1j*np.sqrt(3)*ky/6)) \
         -J3 * (np.exp(-1j*2*np.sqrt(3)*ky/3) + 2*np.cos(kx)*np.exp(1j*np.sqrt(3)*ky/3)) 
    H_k[0,0] = Ak
    H_k[0,1] = Bk
    H_k[1,0] = np.conjugate(Bk)
    H_k[1,1] = Ak
    eigenvalues, eigenvectors = np.linalg.eig(H_k)
    sorted_index = np.argsort(eigenvalues)
    sorted_eigenvalues = eigenvalues[sorted_index]
    sorted_eigenvectors = eigenvectors[:, sorted_index]
    return S * sorted_eigenvalues

X, E, d = cri3.band(kpoints, H_k, Nb=2, Nk=51)
print(d)
plt.plot(X, E, color='red')

# plot phonon
nb_ph = 24
nk_ph = 501
# unit from THz to meV
unit = 4.136
kpath = np.loadtxt('./phonopy/band.dat', comments='#')[:,0].reshape((nb_ph, -1))[0,:]
print(kpath.shape)
band = np.loadtxt('./phonopy/band.dat', comments='#')[:,1].reshape((nb_ph, -1))
xticks = [0.00000000, 0.09521360, 0.14282050, 0.22527870]
xlabels = [r'$\Gamma$', 'K', 'M', r'$\Gamma$']
# here is to rescale the kpath in phonon to magnon kpath
scale1 = (d[1]-d[0]) / (xticks[1]-xticks[0])
kpath[0:nk_ph] = (kpath[0:nk_ph]-xticks[0]) * scale1
scale2 = (d[2]-d[1]) / (xticks[2]-xticks[1])
kpath[nk_ph:2*nk_ph] = kpath[nk_ph-1] + (kpath[nk_ph:2*nk_ph]-xticks[1]) * scale2
scale3 = (d[3]-d[2]) / (xticks[3]-xticks[2])
kpath[2*nk_ph:3*nk_ph] = kpath[2*nk_ph-1] + (kpath[2*nk_ph:3*nk_ph]-xticks[2]) * scale3

for i in range(nb_ph):
    plt.plot(kpath, band[i,:] * unit, c='blue')

# format
fs=16
plt.xlim(d[0],d[-1])
plt.ylim(0,)
plt.axvline(d[1], c='black', lw=0.5)
plt.axvline(d[2], c='black', lw=0.5)
xlabels = [r'$\Gamma$', '$K$', '$M$', r'$\Gamma$']
plt.xticks(d, xlabels, fontsize=fs)
plt.yticks(fontsize=fs)
plt.ylabel('Energy (meV)', fontsize=fs+2)
plt.savefig('bare_CrI3_magnon_phonon.png', dpi=300, bbox_inches='tight')
plt.show()