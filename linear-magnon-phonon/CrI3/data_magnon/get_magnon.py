import numpy as np
import matplotlib.pyplot as plt
from helper import model

def main():
    # load q vectors from phonon calculations
    kpath = 'GKM'
    label = 'DFT_LDA'

    q_points = np.load(f'../data_phonon/q_points_{kpath}.npy')
    nq = q_points.shape[0]
    print(nq)
    print(q_points)
    frequencies = np.zeros((nq, 2))
    eigenvectors = np.zeros((nq, 2, 2), dtype=complex)

    # exchange constants in meV
    # DFT PBE
    #J_list = [2.90, 0.63, -0.16, 0.88]
    # exp: https://journals.aps.org/prx/abstract/10.1103/PhysRevX.8.041028
    #J_list = [2.01, 0.16, -0.08, 0.49*2]
    # DFT LDA
    J_list = [2.16, 0.62, -0.25, 0.49*2]
    # neighbors 
    z_list = [3, 6, 3]

    # go over q vectors
    for iq in range(nq):
        q = q_points[iq]
        freq, eigen = H_k(q, J_list, z_list)
        frequencies[iq, :] = freq
        eigenvectors[iq, :, :] = eigen
    print(frequencies.shape)
    print(eigenvectors.shape)
    np.save(f'eigenvalues_{kpath}_{label}.npy', frequencies)
    np.save(f'eigenvectors_{kpath}_{label}.npy', eigenvectors)

def H_k(k, J_list, z_list):
    """
    for magnon
    up to third-nearest neighbor Hamiltonian
    for each k=[kx, ky], return the eigenvalue
    the basis set is a+, b+
    """
        # CrI3
    # lattice vectors
    a1 = np.array([1.0, 0.0, 0.0])
    a2 = np.array([-1/2.0, np.sqrt(3)/2.0, 0.0])
    a3 = np.array([0.0, 0.0, 1.0])
    S = 3/2

    # unit: meV 
    # parameters from https://journals.aps.org/prb/abstract/10.1103/PhysRevB.106.214424
    # notice there is 1/2 as a prefactor in spin Hamiltonian
    J1, J2, J3, A = (J_list[0], J_list[1], J_list[2], J_list[3])
    z1, z2, z3 = (z_list[0], z_list[1], z_list[2])

    cri3 = model([a1, a2, a3], [J1, J2, J3], [z1, z2, z3], S)
    # reciprocal vectors
    b1, b2, b3 = cri3.get_reciprocal()

    # number of bands
    nb = 2
    k1 = k[0]
    k2 = k[1]
    # Cartesian
    k_C = k1 * b1 + k2 * b2
    kx, ky = k_C[0], k_C[1]
    H_k = np.zeros((nb, nb), dtype=complex)

    Ak = A + z1 * J1 + z2 * J2 + z3 * J3 - 2 * J2 * (np.cos(kx) + 2*np.cos(kx/2)*np.cos(np.sqrt(3)*ky/2))
    Bk = -J1 * (np.exp(1j*np.sqrt(3)*ky/3) + 2*np.cos(kx/2)*np.exp(-1j*np.sqrt(3)*ky/6)) \
         -J3 * (np.exp(-1j*2*np.sqrt(3)*ky/3) + 2*np.cos(kx)*np.exp(1j*np.sqrt(3)*ky/3)) 
    H_k[0,0] = Ak
    H_k[0,1] = Bk
    H_k[1,0] = np.conjugate(Bk)
    H_k[1,1] = Ak
    eigenvalues, eigenvectors = np.linalg.eig(H_k)
    sorted_index = np.argsort(eigenvalues)
    sorted_eigenvalues = eigenvalues[sorted_index]
    sorted_eigenvectors = eigenvectors[:, sorted_index]
    return S * sorted_eigenvalues, sorted_eigenvectors

if __name__ == "__main__":
    main()
