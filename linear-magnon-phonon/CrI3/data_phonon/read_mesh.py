import yaml
import numpy as np

#kpath = 'q31x31'
#kpath = 'q100x1'
kpath = 'GKMG'
with open(f'../phonopy/{kpath}.yaml') as f:
    data = yaml.safe_load(f)

phonon = data['phonon']

# save the q poionts
q_positions = []
for entry in phonon:
    q = entry['q-position']
    q_positions.append(q)

q_positions = np.array(q_positions)
np.save(f'q_points_{kpath}.npy', q_positions)

# save the band energy and eigenvector
frequencies = []
eigenvectors = []
for entry in phonon:
    bands = entry['band']
    for band in bands:
        frequency = band['frequency']
        eigenvector = band['eigenvector']
        # remove the negative frequency
        if frequency < 0.00:
            frequency = 0.00
        frequencies.append(frequency)
        eigenvectors.append(eigenvector)
frequencies = np.array(frequencies)
eigenvectors = np.array(eigenvectors)

nq = data['nqpoint']
frequencies = frequencies.reshape((nq, -1))
print(frequencies.shape)

eigenvectors = eigenvectors.reshape((nq, -1, 8, 3, 2))
print(eigenvectors.shape)

# unit from THz to meV
unit = 4.136
np.save(f'eigenvalues_{kpath}.npy', frequencies * unit)
np.save(f'eigenvectors_{kpath}.npy', eigenvectors)
