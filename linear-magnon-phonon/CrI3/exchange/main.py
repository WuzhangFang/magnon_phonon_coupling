import numpy as np
from helper import read_poscar, get_supercell, get_supercell_neighbors, get_neighbor_index, get_J, get_INCAR
# Based on the distance, find the neighbors

# read the coordinates in Cartesian
a1, a2, a3, supercell_coord = read_poscar('POSCAR-3x3x1-C.vasp')
#a1, a2, a3, supercell_coord = read_poscar('POSCAR-C.vasp')

supercell = get_supercell(supercell_coord)
supercell_neighbors = get_supercell_neighbors(supercell, a1, a2)

# nearest neighbors
nn1 = 4.04245
nn2 = 7.00173
nn3 = 8.08490

z_up = np.array([0, 0, 1]) 
z_dw = np.array([0, 0, -1])

nn1_index = get_neighbor_index(supercell, supercell_neighbors, nn1)
nn2_index = get_neighbor_index(supercell, supercell_neighbors, nn2)
nn3_index = get_neighbor_index(supercell, supercell_neighbors, nn3)

spins_list = []
#1
spins = [z_up, z_up, z_up, z_up, z_up, z_up] + [z_up] * 12
spins_list.append(spins)
#2
spins = [z_up, z_dw, z_up, z_up, z_up, z_up] + [z_up] * 12
spins_list.append(spins)
#3
spins = [z_up, z_dw, z_dw, z_up, z_up, z_up] + [z_up] * 12
spins_list.append(spins)
#4
spins = [z_up, z_dw, z_dw, z_dw, z_up, z_up] + [z_up] * 12
spins_list.append(spins)
#5
spins = [z_up, z_dw, z_dw, z_dw, z_dw, z_up] + [z_up] * 12
spins_list.append(spins)
#6
spins = [z_up, z_dw, z_dw, z_dw, z_dw, z_dw] + [z_up] * 12
spins_list.append(spins)
#7 
spins = [z_dw, z_dw, z_dw, z_dw, z_dw, z_dw] + [z_up] * 12
spins_list.append(spins)


E1 = []
E2 = []
E3 = []
for i,spins in enumerate(spins_list):
    get_INCAR(i, spins)
    E1.append(get_J(spins, nn1_index))
    E2.append(get_J(spins, nn2_index))
    E3.append(get_J(spins, nn3_index))

E1 = np.array(E1)
E2 = np.array(E2)
E3 = np.array(E3)

print(E1)
print(E2)
print(E3)

