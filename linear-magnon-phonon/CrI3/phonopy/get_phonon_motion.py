import numpy as np

# File path
file_path = 'gamma.ascii'

# List to store extracted data
data = []

# Read and process the file
with open(file_path, 'r') as file:
    lines = file.readlines()
    for i, line in enumerate(lines):
        if line.startswith("#metaData: qpt"):
            # Extract the block (next 8 lines after `#metaData: qpt`)
            block = lines[i + 1:i + 9]
            # Combine all lines into a single string, remove newlines, backslashes, and '#' symbols, then split by semicolon
            block_data = ''.join(block).replace("\\", "").replace("#", "").replace("\n", "").split(";")
            # Convert strings to floats and append to data
            data.append([float(x) for x in block_data if x.strip()])

# Convert the data list to a numpy array
data_array = np.array(data)
#print(data_array.shape)

# mode 4
i = 4 - 1
print(f'----------{i+1} mode-------------')
print(data_array[i].reshape(8, -1)*50)

# mode 11
i = 11 - 1
print(f'----------{i+1} mode-------------')
print(data_array[i].reshape(8, -1)*50)

# mode 13
i = 13 - 1
print(f'----------{i+1} mode-------------')
print(data_array[i].reshape(8, -1)*50)

# mode 8
i = 8 - 1
print(f'----------{i+1} mode-------------')
print(data_array[i].reshape(8, -1)*50)

# mode 15
i = 15 - 1
print(f'----------{i+1} mode-------------')
print(data_array[i].reshape(8, -1)*50)

# mode 20
i = 20 - 1
print(f'----------{i+1} mode-------------')
print(data_array[i].reshape(8, -1)*50)
