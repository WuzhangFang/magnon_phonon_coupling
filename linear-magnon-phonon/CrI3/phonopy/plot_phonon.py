import numpy as np
import matplotlib.pyplot as plt

data1 = np.loadtxt('./band_LO-TO.dat', comments="#")
data2 = np.loadtxt('./band_no_LO-TO.dat', comments="#")

nbnd = 24

k1 = data1[:,0].reshape((nbnd, -1))[0,:]
k2 = data2[:,0].reshape((nbnd, -1))[0,:]

e1 = data1[:,1].reshape((nbnd, -1))
e2 = data2[:,1].reshape((nbnd, -1))

# unit from THz to meV
unit = 4.136

for i in range(nbnd):
    plt.plot(k1, e1[i,:]*unit, color='blue', label='w/ LOTO')
    plt.plot(k2, e2[i,:]*unit, color='red', label='w/o LOTO')

xticks = [0.00000000, 0.09521360, 0.14282050, 0.22527870]
plt.xlim(xticks[0], xticks[-1])
xlabels = [r'$\Gamma$', 'K', 'M', r'$\Gamma$']
plt.xticks(xticks, xlabels)

for x in xticks:
    plt.axvline(x, lw=0.5, c='black')

plt.ylabel('Energy (meV)')
plt.show()