import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from helper import get_reciprocal
from scipy.interpolate import griddata
np.set_printoptions(precision=6)

# Reciprocal lattice in Cartesian coordinates
a = 7.0017278277323989
a1 = np.array([a, 0.0, 0.0])
a2 = np.array([-a / 2.0, np.sqrt(3) * a / 2.0, 0.0])
a3 = np.array([0.0, 0.0, 23.0])
b1, b2, b3 = get_reciprocal([a1, a2, a3])

kpath = 'q31x31'
qpoints = np.load(f'./data_phonon/q_points_{kpath}.npy')
# Convert q points to Cartesian coordinates for 31x31 grid
q_x = [q1 * b1[0] + q2 * b2[0] for q1, q2, _ in qpoints]
q_y = [q1 * b1[1] + q2 * b2[1] for q1, q2, _ in qpoints]

# Create a grid for interpolation
grid_x, grid_y = np.meshgrid(
    np.linspace(min(q_x), max(q_x), 150),   # 100 points along x
    np.linspace(min(q_y), max(q_y), 150)    # 100 points along y
)

# Set up the 3D plot
fig = plt.figure(figsize=(10, 8))
ax = fig.add_subplot(111, projection='3d')
fs = 14

# Magnon and phonon mode indices and coupling values
i_mag = 0
i_ph = 15

# LDA
label = 'DFT_LDA'
lambda_LDA = np.load(f'./LDA/lambda_{kpath}_{label}.npy')
c_LDA = np.abs(lambda_LDA[:, i_mag, i_ph])

# PBE
label = 'DFT'
lambda_PBE = np.load(f'./supercell_3x3/lambda_{kpath}_{label}.npy')
c_PBE = np.abs(lambda_PBE[:, i_mag, i_ph])


# plot LDA
# Interpolate values on the grid
grid_z = griddata((q_x, q_y), c_LDA, (grid_x, grid_y), method='cubic')
# Plot the surface with interpolated data
surf = ax.plot_surface(
    grid_x, grid_y, grid_z, 
    cmap='Blues',            # Colormap for visual clarity
    edgecolor='none',        # Remove edges for smooth look
    alpha=0.7,               # Transparency for better visual clarity
    shade=True,
    rstride=1, cstride=1,    # Stride options for a smooth surface
    antialiased=True,
    vmin=0.0, vmax=0.30,
    label='LDA'
)

# plot PBE
# Interpolate values on the grid
grid_z = griddata((q_x, q_y), c_PBE, (grid_x, grid_y), method='cubic')
# Plot the surface with interpolated data
surf = ax.plot_surface(
    grid_x, grid_y, grid_z, 
    cmap='Reds',            # Colormap for visual clarity
    edgecolor='none',        # Remove edges for smooth look
    alpha=0.7,               # Transparency for better visual clarity
    shade=True,
    rstride=1, cstride=1,    # Stride options for a smooth surface
    antialiased=True,
    vmin=0.0, vmax=0.30,
    label='PBE'
)

# Labels and display
ax.legend()
ax.set_xlabel('$q_x$', fontsize=fs)
ax.set_ylabel('$q_y$', fontsize=fs)
ax.tick_params(labelsize=fs-2)

#plt.savefig(f'BZ_3D_{i_mag}_{i_ph}.png', dpi=300, bbox_inches='tight')
#plt.savefig(f'BZ_3D_{i_mag}_{i_ph}.pdf', dpi=300, bbox_inches='tight')
plt.show()
