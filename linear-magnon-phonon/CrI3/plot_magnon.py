import numpy as np
import matplotlib.pyplot as plt
from helper import model

# CrI3
# lattice vectors
a1 = np.array([1.0, 0.0, 0.0])
a2 = np.array([-1/2.0, np.sqrt(3)/2.0, 0.0])
a3 = np.array([0.0, 0.0, 1.0])

S = 3/2
# unit: meV
J = np.array([3.26, 0.71, -0.18, 1.97/2])
J_scaled = J * 2 / S**2
print(J_scaled)
z1, z2, z3 = (3, 6, 3)
# J fitted from inelastic neutron scattering
# exp: https://journals.aps.org/prx/abstract/10.1103/PhysRevX.8.041028
J_list = [2.01, 0.16, -0.08, 0.49*2]

cri3_DFT = model([a1, a2, a3], J_scaled, [z1, z2, z3], S)
cri3_exp = model([a1, a2, a3], J_list, [z1, z2, z3], S)
# reciprocal vectors
b1, b2, b3 = cri3_DFT.get_reciprocal()

# plot band structure
G = 0.0 * b1 + 0.0 * b2
K = 0.333333 * b1 + 0.333333 * b2
M = 0.5 * b1 + 0.0 * b2
kpoints = np.array([G, K, M, G])

def H_k(k, J_list, z_list, S):
    """
    for magnon
    up to third-nearest neighbor Hamiltonian
    for each k=[kx, ky], return the eigenvalue
    the basis set is a+, b+ (row) a, b (column)
    """
    # number of bands
    nb = 2
    kx = k[0]
    ky = k[1]
    H_k = np.zeros((nb, nb), dtype=complex)
    J1, J2, J3, A = J_list
    z1, z2, z3 = z_list
    Ak = A + z1 * J1 + z2 * J2 + z3 * J3 - 2 * J2 * (np.cos(kx) + 2*np.cos(kx/2)*np.cos(np.sqrt(3)*ky/2))
    Bk = -J1 * (np.exp(1j*np.sqrt(3)*ky/3) + 2*np.cos(kx/2)*np.exp(-1j*np.sqrt(3)*ky/6)) \
         -J3 * (np.exp(-1j*2*np.sqrt(3)*ky/3) + 2*np.cos(kx)*np.exp(1j*np.sqrt(3)*ky/3)) 
    H_k[0,0] = Ak
    H_k[0,1] = Bk
    H_k[1,0] = np.conjugate(Bk)
    H_k[1,1] = Ak
    eigenvalues, eigenvectors = np.linalg.eig(H_k)
    sorted_index = np.argsort(eigenvalues)
    sorted_eigenvalues = eigenvalues[sorted_index]
    sorted_eigenvectors = eigenvectors[:, sorted_index]
    return S * sorted_eigenvalues

fs = 12  # Font size
lw = 1   # Line width
X1, E1, d1 = cri3_DFT.band(kpoints, H_k, Nb=2, Nk=51)
X2, E2, d2 = cri3_exp.band(kpoints, H_k, Nb=2, Nk=51)
print(E1.shape)
fig, ax = plt.subplots(figsize=(4,3))
ax.plot(X1, E1[:,0], lw=lw, color='red', label='DFT')
ax.plot(X2, E2[:,0], lw=lw, color='blue', label='Exp')
ax.plot(X1, E1[:,1], lw=lw, color='red')
ax.plot(X2, E2[:,1], lw=lw, color='blue')
# format

ax.set_xlim(d1[0],d1[-1])
ax.set_ylim(0,)
ax.axvline(d1[1], c='black', ls='--', lw=lw/3)
ax.axvline(d1[2], c='black', ls='--', lw=lw/3)
xlabels = [r'$\Gamma$', 'K', 'M', r'$\Gamma$']
ax.set_xticks(d1, xlabels)
ax.set_ylabel('Energy (meV)', fontsize=fs)
ax.tick_params(labelsize=fs-2)
ax.legend(fontsize=fs-2, facecolor='none', edgecolor='none', bbox_to_anchor=(0.7, 0.9))
plt.savefig('CrI3_magnon.png', dpi=300, bbox_inches='tight')
plt.savefig('CrI3_magnon.pdf', dpi=300, bbox_inches='tight')
plt.show()