import numpy as np
from helper import get_derivative

# 18 Cr and 54 I atoms in a supercell
num_primitive = 8
num_atom = 18 + 54

# supercell to primitive cell
s2p = [0, 1] * 9 + [2] * 9 + [3] * 9 + [4] * 9 + [5] * 9 + [6] * 9 + [7] * 9
s2p = np.array(s2p, dtype=int)

# get the J' for Cr1 and Cr2 in a 3x3 supercell
# shape: Cr1/Cr2, num_atom, x/y/z
J_prime_real = np.zeros((2, num_atom, 3))
J_prime_imag = np.zeros((2, num_atom, 3))

# sum over nearest neighbors, first three: nearest, second six: second-nearest
# Cr1
for pair in ['1-2', '1-6', '1-8', '1-3', '1-5', '1-7', '1-9', '1-13', '1-17']:
    J_prime_real[0,:,:] += get_derivative(f"./X-{pair}.dat", num_atom)
    J_prime_imag[0,:,:] += get_derivative(f"./Y-{pair}.dat", num_atom)
# Cr2
for pair in ['2-1', '2-3', '2-13', '2-4', '2-6', '2-8', '2-10', '2-14', '2-18']:
    J_prime_real[1,:,:] += get_derivative(f"./X-{pair}.dat", num_atom)
    J_prime_imag[1,:,:] += get_derivative(f"./Y-{pair}.dat", num_atom)

# reformulate J'
# s:0/1, t:0-7, num_atom, xyz components
# Eq. (35)
# real: Jxz, imag: Jyz
J_prime_st_R = np.zeros((2, num_primitive, num_atom, 3), dtype=complex)
for i in range(num_atom):
    pos = s2p[i]
    # need to calculate separately for Cr1 and Cr2
    J_prime_st_R[0,pos,i,:] = J_prime_real[0,i,:] - 1j*J_prime_imag[0,i,:]
    J_prime_st_R[1,pos,i,:] = J_prime_real[1,i,:] - 1j*J_prime_imag[1,i,:]   

print(J_prime_st_R.shape)
np.save('J_prime_st_R.npy', J_prime_st_R)
