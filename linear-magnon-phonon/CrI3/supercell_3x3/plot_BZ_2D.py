import numpy as np
import matplotlib.pyplot as plt
from helper import get_reciprocal
np.set_printoptions(precision=6)

# plot the lambada on a grid in BZ

# grid 3x3:exact for Fourier transform in a 3x3 supercell
system = 'q3x3'
label = 'DFT'
lambda_q_3 = np.load(f'lambda_{system}_{label}.npy')
print('shape of lambda_q: ', lambda_q_3.shape)
phonon_folder = '../data_phonon'
q_3 = np.load(f'{phonon_folder}/q_points_{system}.npy')
print('shape of q_points: ', q_3.shape)

# grid interpolation
system = 'q31x31'
label = 'DFT'
lambda_q_27 = np.load(f'lambda_{system}_{label}.npy')
print('shape of lambda_q: ', lambda_q_27.shape)
phonon_folder = '../data_phonon'
q_27 = np.load(f'{phonon_folder}/q_points_{system}.npy')
print('shape of q_points: ', q_27.shape)

# reciprocal lattice in Cartesian coordinates
a = 7.0017278277323989
a1 = np.array([a, 0.0, 0.0])
a2 = np.array([-a / 2.0, np.sqrt(3) * a / 2.0, 0.0])
a3 = np.array([0.0, 0.0, 23.0])
b1, b2, b3 = get_reciprocal([a1, a2, a3])

# magnon and phonon mode
i_mag = 0
i_ph = 15
# coupling
cvalues = np.abs(lambda_q_27[:, i_mag, i_ph])

fig, ax = plt.subplots()

# plot q grid
for iq in range(q_27.shape[0]):
    q1, q2, q3 = q_27[iq]
    q = q1 * b1 + q2 * b2 + q3 * b3
    a = ax.scatter(q[0], q[1], c=cvalues[iq], cmap='Blues', s=125, alpha=1.0, 
                        edgecolors='none', vmin=0.0, vmax=max(cvalues))

# Plotting q points on 3x3 grid
for iq in range(q_3.shape[0]):
    q1, q2, q3 = q_3[iq]
    q = q1 * b1 + q2 * b2 + q3 * b3
    ax.scatter(q[0], q[1], color='red')

plt.show()


# reference
# # magnon and phonon mode
# n_mag = 2
# n_ph = 24

# i_mag = 0
# # Create a 3x8 grid for plotting
# fig, ax = plt.subplots(3, 8, figsize=(48, 18), gridspec_kw={'hspace': 0.05, 'wspace': 0.05})
# ax = ax.flatten()
# for i_ph in range(n_ph):
#     idx = i_mag * n_ph + i_ph  # Calculate the index for the grid
#     cvalues = np.abs(lambda_q_27[:, i_mag, i_ph])
#     print(cvalues)

#     # Plotting q points on a grid
#     for iq in range(q_27.shape[0]):
#         q1, q2, q3 = q_27[iq]
#         q = q1 * b1 + q2 * b2 + q3 * b3
#         a = ax[idx].scatter(q[0], q[1], c=cvalues[iq], cmap='Blues', s=125, alpha=1.0, 
#                             edgecolors='none', vmin=0.0, vmax=max(cvalues))
#         ax[idx].set_xticks([])
#         ax[idx].set_yticks([])
    
#     # Plotting q points on 3x3 grid
#     for iq in range(q_3.shape[0]):
#         q1, q2, q3 = q_3[iq]
#         q = q1 * b1 + q2 * b2 + q3 * b3
#         ax[idx].scatter(q[0], q[1], color='red')

# plt.savefig(f'BZ_figures_{system}_{label}_{i_mag}.png', dpi=300, bbox_inches='tight')

# i_mag = 1
# # Create a 3x8 grid for plotting
# fig, ax = plt.subplots(3, 8, figsize=(48, 18), gridspec_kw={'hspace': 0.05, 'wspace': 0.05})
# ax = ax.flatten()
# for i_ph in range(n_ph):
#     idx = (i_mag - 1) * n_ph + i_ph  # Calculate the index for the grid
#     cvalues = np.abs(lambda_q_27[:, i_mag, i_ph])
#     print(cvalues)

#     # Plotting q points on a grid
#     for iq in range(q_27.shape[0]):
#         q1, q2, q3 = q_27[iq]
#         q = q1 * b1 + q2 * b2 + q3 * b3
#         a = ax[idx].scatter(q[0], q[1], c=cvalues[iq], cmap='Blues', s=125, alpha=1.0, 
#                             edgecolors='none', vmin=0.0, vmax=max(cvalues))
#         ax[idx].set_xticks([])
#         ax[idx].set_yticks([])
    
#     # Plotting q points on 3x3 grid
#     for iq in range(q_3.shape[0]):
#         q1, q2, q3 = q_3[iq]
#         q = q1 * b1 + q2 * b2 + q3 * b3
#         ax[idx].scatter(q[0], q[1], color='red')

# plt.savefig(f'BZ_figures_{system}_{label}_{i_mag}.png', dpi=300, bbox_inches='tight')