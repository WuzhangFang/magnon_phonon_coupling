import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from helper import get_reciprocal
from matplotlib import cm  # For color map handling
from scipy.interpolate import griddata

np.set_printoptions(precision=6)

# Set up system information for data loading
system_3x3 = 'q3x3'
label = 'DFT'
lambda_q_3 = np.load(f'lambda_{system_3x3}_{label}.npy')
q_3 = np.load(f'../data_phonon/q_points_{system_3x3}.npy')

system_31x31 = 'q31x31'
lambda_q_27 = np.load(f'lambda_{system_31x31}_{label}.npy')
q_27 = np.load(f'../data_phonon/q_points_{system_31x31}.npy')

# Reciprocal lattice in Cartesian coordinates
a = 7.0017278277323989
a1 = np.array([a, 0.0, 0.0])
a2 = np.array([-a / 2.0, np.sqrt(3) * a / 2.0, 0.0])
a3 = np.array([0.0, 0.0, 23.0])
b1, b2, b3 = get_reciprocal([a1, a2, a3])

# Magnon and phonon mode indices and coupling values
i_mag = 0
i_ph = 15
c_27 = np.abs(lambda_q_27[:, i_mag, i_ph])
c_3 = np.abs(lambda_q_3[:, i_mag, i_ph])

# Convert q points to Cartesian coordinates for 31x31 grid
q_x = [q1 * b1[0] + q2 * b2[0] for q1, q2, q3 in q_27]
q_y = [q1 * b1[1] + q2 * b2[1] for q1, q2, q3 in q_27]

# Create a grid for interpolation
grid_x, grid_y = np.meshgrid(
    np.linspace(min(q_x), max(q_x), 150),   # 100 points along x
    np.linspace(min(q_y), max(q_y), 150)    # 100 points along y
)

# Interpolate c_27 values on the grid
grid_z = griddata((q_x, q_y), c_27, (grid_x, grid_y), method='cubic')

# Set up the 3D plot
fig = plt.figure(figsize=(10, 8))
ax = fig.add_subplot(111, projection='3d')
fs = 14

# Plot the surface
surf = ax.plot_surface(
    grid_x, grid_y, grid_z, 
    cmap='coolwarm',        # Diverging colormap
    edgecolor='none', 
    alpha=0.5,              # Transparency
    shade=True
)

# Add red points with enhanced style
ax.scatter(
    [q1 * b1[0] + q2 * b2[0] for q1, q2, q3 in q_3],
    [q1 * b1[1] + q2 * b2[1] for q1, q2, q3 in q_3],
    c_3,
    color='red',
    s=100,                # Larger size
    alpha=1.0,
    edgecolors='black',   # Black edges for contrast
    linewidths=1.0
)

# Add shadows (projections) of red points on z=0 plane
ax.scatter(
    [q1 * b1[0] + q2 * b2[0] for q1, q2, q3 in q_3],
    [q1 * b1[1] + q2 * b2[1] for q1, q2, q3 in q_3],
    np.zeros(len(c_3)),   # Projection on z=0
    color='gray',
    s=50,
    alpha=0.5,
    edgecolors='none'
)

# Add color bar with custom limits
cbar = fig.colorbar(surf, ax=ax, shrink=0.5, aspect=10)
cbar.set_label('|$\lambda$| (mev)', fontsize=fs, rotation=270, labelpad=25)
#cbar.set_clim(0, 0.3)     # Adjust to match data range

# Labels and axis ticks
ax.set_xlabel('$q_x$', fontsize=fs)
ax.set_ylabel('$q_y$', fontsize=fs)
ax.tick_params(labelsize=fs-2)

# Adjust viewing angle
ax.view_init(elev=30, azim=60)

# Save and display
#plt.savefig(f'BZ_3D_{i_mag}_{i_ph}_improved.png', dpi=300, bbox_inches='tight')
plt.show()
