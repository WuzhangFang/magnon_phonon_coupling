import numpy as np
import matplotlib.pyplot as plt

# Load data for q100x1
system1 = 'q100x1'
label1 = 'DFT'
lambda_q_3_1 = np.load(f'lambda_{system1}_{label1}.npy')
phonon_folder = '../data_phonon'
q_3_1 = np.load(f'{phonon_folder}/q_points_{system1}.npy')

# Load data for q3x3
system2 = 'q3x3'
label2 = 'DFT'
lambda_q_3_2 = np.load(f'lambda_{system2}_{label2}.npy')
q_3_2 = np.load(f'{phonon_folder}/q_points_{system2}.npy')

# Set up subplots with vertical layout
fig, axs = plt.subplots(2, 1, figsize=(8, 9))
fs=16

# Plot for (a) - q100x1 (Gamma point)
iq1 = 1  # plot the Gamma point
bar_width = 0.35
x1 = np.arange(len(lambda_q_3_1[iq1, 0, :])) + 1
axs[0].bar(x1 - bar_width / 2, np.abs(lambda_q_3_1[iq1, 0, :]), width=bar_width, label='Magnon acoustic mode')
axs[0].bar(x1 + bar_width / 2, np.abs(lambda_q_3_1[iq1, 1, :]), width=bar_width, label='Magnon optic mode')
axs[0].set_xticks(np.arange(1, 25, 1))
axs[0].set_yticks([float(f'{y:.2f}') for y in np.arange(0.00, 0.60, 0.10)])
axs[0].set_xlabel('Phonon mode #', fontsize=fs)
axs[0].set_ylabel(r'$|\lambda|$ (meV)', fontsize=fs)
axs[0].legend(frameon=False, fontsize=fs)
axs[0].text(-0.07, 0.96, '(a)', transform=axs[0].transAxes, fontsize=fs, va='top', ha='right')
axs[0].tick_params(labelsize=fs-2)

# Plot for (b) - q3x3 (K point)
iq2 = 4  # plot the K point
x2 = np.arange(len(lambda_q_3_2[iq2, 0, :])) + 1
axs[1].bar(x2 - bar_width / 2, np.abs(lambda_q_3_2[iq2, 0, :]), width=bar_width, label='Magnon acoustic mode')
axs[1].bar(x2 + bar_width / 2, np.abs(lambda_q_3_2[iq2, 1, :]), width=bar_width, label='Magnon optic mode')
axs[1].set_xticks(np.arange(1, 25, 1))
axs[1].set_yticks([float(f'{y:.2f}') for y in np.arange(0.00, 0.60, 0.10)])
axs[1].set_xlabel('Phonon mode #', fontsize=fs)
axs[1].set_ylabel(r'$|\lambda|$ (meV)', fontsize=fs)
axs[1].legend(frameon=False, fontsize=fs)
axs[1].text(-0.07, 0.96, '(b)', transform=axs[1].transAxes, fontsize=fs, va='top', ha='right')
axs[1].tick_params(labelsize=fs-2)

# Adjust layout and save
plt.tight_layout()
plt.savefig('CrI3_coupling_G_K.png', dpi=300, bbox_inches='tight')
plt.savefig('CrI3_coupling_G_K.pdf', dpi=300, bbox_inches='tight')
plt.show()