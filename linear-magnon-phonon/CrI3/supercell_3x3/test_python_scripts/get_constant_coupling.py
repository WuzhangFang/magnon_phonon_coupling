import numpy as np
np.set_printoptions(precision=6)

# set the coupling constant to 0.2 meV everywhere
# check the implementation of diagonalizing Hamilton

# load the q grids
kpath = 'GKM'
phonon_folder = '../data_phonon'
q_points = np.load(f'{phonon_folder}/q_points_{kpath}.npy')
nq = q_points.shape[0]
print('q vectors: \n', q_points)

# calculate linear magnon-phonon coupling
# (9, 2, 24), 9 q-vectors, 2 magnon modes and 24 phonon modes
lambda_q = np.zeros((nq, 2, 24), dtype=complex)

for iq in range(nq):
    for mu in range(2):
        for nu in range(24):
            lambda_q[iq, mu, nu] = 0.2

print('shape of lambda_q: ', lambda_q.shape)
np.save(f'lambda_{kpath}_constant.npy', lambda_q)