import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap

# get the gap openning at K by diaognalizing the Hamiltonian

kpath = 'GKM'
label = 'exp'
magnon_folder = '../data_magnon'
phonon_folder = '../data_phonon'

# load eigenvalues and eigenvectors of phonon
phonon_energies = np.load(f'{phonon_folder}/eigenvalues_{kpath}.npy')
#print('shape of phonon energies', phonon_energies.shape)

# load eigenvalues and eigenvectors of magnon
magnon_energies = np.load(f'{magnon_folder}/eigenvalues_{kpath}_{label}.npy')
#print('shape of magnon energies', magnon_energies.shape)

# load coupling constants
lambda_q = np.load(f'lambda_{kpath}_{label}.npy')
#print('shape of couling constants', lambda_q.shape)

# load q points
q_points = np.load(f'{phonon_folder}/q_points_{kpath}.npy')

nq = phonon_energies.shape[0]
nb_ph = phonon_energies.shape[1]
nb_mag = magnon_energies.shape[1]
nb = nb_ph + nb_mag

magnon_weight = np.zeros((nq,nb))
eigenvalues = np.zeros((nq,nb))

iq = 10

# construct the Hamiltonian
H_k = np.zeros((nb, nb), dtype=complex)
# magnon
for ib in range(nb_mag):
    H_k[ib, ib] = magnon_energies[iq, ib]
# phonon
for ib in range(nb_ph):
    H_k[ib+nb_mag,ib+nb_mag] = phonon_energies[iq, ib]
# coupling
for i in range(nb_mag):
    for j in range(nb_ph):
        H_k[i,j+nb_mag] = lambda_q[iq,i,j]
        H_k[j+nb_mag,i] = lambda_q[iq,i,j].conj()

print(H_k)

eig, eigv = np.linalg.eig(H_k)
# calculate magnon weight
weight = np.linalg.norm(eigv[0:2,:], axis=0)
eigenvalues[iq,:] = np.real(eig)
magnon_weight[iq, :] = weight

print(phonon_energies[iq, :])
print(magnon_energies[iq, :])
print(eigenvalues[iq, :])
