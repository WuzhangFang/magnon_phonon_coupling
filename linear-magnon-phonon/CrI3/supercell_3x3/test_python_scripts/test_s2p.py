import numpy as np

s2p = [0, 1] * 9 + [2] * 9 + [3] * 9 + [4] * 9 + [5] * 9 + [6] * 9 + [7] * 9
s2p = np.array(s2p, dtype=int)

# test
i = 27 + 18 - 1 
result = s2p[i]-1
expected_result = 3
try:
    assert result == expected_result, f"Test failed: expected {expected_result}, got {result}"
    print("Test passed")
except AssertionError as e:
    print(e)

# test
i = 54 + 18 - 1 
result = s2p[i]-1
expected_result = 6
try:
    assert result == expected_result, f"Test failed: expected {expected_result}, got {result}"
    print("Test passed")
except AssertionError as e:
    print(e)

# test
i = 35 + 18 - 1 
result = s2p[i]-1
expected_result = 4
try:
    assert result == expected_result, f"Test failed: expected {expected_result}, got {result}"
    print("Test passed")
except AssertionError as e:
    print(e)

# test
i = 37 + 18 - 1 
result = s2p[i]-1
expected_result = 5
try:
    assert result == expected_result, f"Test failed: expected {expected_result}, got {result}"
    print("Test passed")
except AssertionError as e:
    print(e)

# supercell to primitive cell
s2p = [0, 1] * 9 + [2] * 9 + [3] * 9 + [4] * 9 + [5] * 9 + [6] * 9 + [7] * 9
s2p = np.array(s2p, dtype=int)

I_index = np.array([1, 10, 19, 28, 37, 46], dtype=int)
I_s2p = I_index + 18 -1
p2s = [0, 1] + list(I_s2p)
p2s = np.array(p2s, dtype=int)

print(p2s)
print(s2p[p2s])