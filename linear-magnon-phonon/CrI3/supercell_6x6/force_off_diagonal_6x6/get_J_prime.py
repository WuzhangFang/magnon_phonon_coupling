import numpy as np
from helper import get_derivative
import yaml

# 72 Cr and 216 I atoms in a supercell
num_primitive = 8
num_atom = 72 + 216
num_cells = 6 * 6

# supercell to primitive cell
# use phonopy to read the index
data = yaml.safe_load(open("../phonopy_disp.yaml"))
s2p = []
for i in range(num_atom):
    s2p.append((data['supercell']['points'][i]['reduced_to']-1) / num_cells)
s2p = np.array(s2p, dtype=int)
print(s2p)

# get the J' for Cr1 and Cr2 in a 3x3 supercell
# shape: Cr1/Cr2, num_atom, x/y/z
J_prime_real = np.zeros((2, num_atom, 3))
J_prime_imag = np.zeros((2, num_atom, 3))

# sum over nearest neighbors, first three: nearest, second six: second-nearest
# Cr1
for pair in ['1-37', '1-38', '1-67', '1-2', '1-6', '1-7', '1-8', '1-31', '1-36']:
    J_prime_real[0,:,:] += get_derivative(f"./X-{pair}.dat", num_atom)
    J_prime_imag[0,:,:] += get_derivative(f"./Y-{pair}.dat", num_atom)
# Cr2
for pair in ['37-1', '37-6', '37-7', '37-38', '37-42', '37-43', '37-44', '37-67', '37-72']:
    J_prime_real[1,:,:] += get_derivative(f"./X-{pair}.dat", num_atom)
    J_prime_imag[1,:,:] += get_derivative(f"./Y-{pair}.dat", num_atom)

# reformulate J'
# s:0/1, t:0-7, num_atom, xyz components
# Eq. (35)
# real: Jxz, imag: Jyz
J_prime_st_R = np.zeros((2, num_primitive, num_atom, 3), dtype=complex)
for i in range(num_atom):
    pos = s2p[i]
    # need to calculate separately for Cr1 and Cr2
    J_prime_st_R[0,pos,i,:] = J_prime_real[0,i,:] - 1j*J_prime_imag[0,i,:]
    J_prime_st_R[1,pos,i,:] = J_prime_real[1,i,:] - 1j*J_prime_imag[1,i,:]   

print(J_prime_st_R.shape)
np.save('J_prime_st_R.npy', J_prime_st_R)
