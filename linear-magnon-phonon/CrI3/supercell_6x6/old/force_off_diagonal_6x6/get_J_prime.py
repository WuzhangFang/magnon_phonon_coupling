import numpy as np
from helper import get_derivative

# 72 Cr atoms in a 6x6x1 supercell
num_atom = 72

# each atom has an index of unit cell and position inside the unit cell
position_index = np.array([0] * 36 + [1] * 36)
print(position_index)

# get the J' for Cr1 and Cr2 in a 3x3 supercell
# shape: Cr1/Cr2, num_atom, x/y/z
J_prime_real = np.zeros((2, num_atom, 3))
J_prime_imag = np.zeros((2, num_atom, 3))

# sum over nearest neighbors
# Cr1
for pair in ['1-37', '1-42', '1-43']:
    J_prime_real[0,:,:] += get_derivative(f"./X-{pair}.dat", num_atom)
    J_prime_imag[0,:,:] += get_derivative(f"./Y-{pair}.dat", num_atom)
# Cr2
for pair in ['37-1', '37-2', '37-31']:
    J_prime_real[1,:,:] += get_derivative(f"./X-{pair}.dat", num_atom)
    J_prime_imag[1,:,:] += get_derivative(f"./Y-{pair}.dat", num_atom)

# reformulate J'
# s, t, num_atom, xyz components
# Eq. (35)
# real: Jxz, imag: Jyz
J_prime_st_R = np.zeros((2, 2, num_atom, 3), dtype=complex)
for i in range(num_atom):
    pos = position_index[i]
    # here need a sum of j, j is index for nearest neighbor
    # need to calculate separately for Cr1 and Cr2
    J_prime_st_R[0, pos, i, :] = J_prime_real[0,i,:] - 1j*J_prime_imag[0,i,:]
    J_prime_st_R[1, pos, i, :] = J_prime_real[1,i,:] - 1j*J_prime_imag[1,i,:]   

print(J_prime_st_R.shape)
np.save('J_prime_st_R.npy', J_prime_st_R)
