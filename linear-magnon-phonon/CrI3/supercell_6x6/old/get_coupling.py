import numpy as np
import matplotlib.pyplot as plt
from helper import get_reciprocal, find_shortest
import itertools
np.set_printoptions(precision=6)

# phonon frequencies and eigenvectors are calculated in phonon_GKM
# magnon frequencies and eigenvectors are calculated in magnon_exp_GKM
# outline: (1) Fourier transform of J_prime_st_R
#          (2) get the couping constant using J_prime_q and eigenvectors of magnon and phonon
# 07/11/2024: modify the phase factor to include the supercell size

# kpath = 'GKM'
# label = 'exp'
kpath = 'q3x3'
label = 'DFT'
magnon_folder = '../data_magnon'
phonon_folder = '../data_phonon'

# 18 Cr atoms in a supercell
primitive_num = 2
supercell_num = 72

# supercell to primitive cell
s2p = np.array([0] * 36 + [1] * 36, dtype=int)
p2s = np.array([0, 36], dtype=int)

# load the coordinates
supercell_coord = np.load('coordinates_direct.npy')[:supercell_num]
print(supercell_coord.shape)

# generate the Lvectors
values = [-1, 0, 1]
# Use itertools.product to generate all combinations
combinations = list(itertools.product(values, repeat=3))
Lvectors = np.array(combinations)

multi = np.zeros((primitive_num, supercell_num), dtype=int)
index = np.zeros((primitive_num, supercell_num), dtype=int)
# relative distance between atomi and atomj in the supercell
vectors = np.zeros((primitive_num, supercell_num, 3))
# add supercell vectors
vectors_L = []
counter = 0

for i in range(primitive_num):
    for j in range(supercell_num):
        # primitive to supercell
        # store the vector
        R = supercell_coord[j] - supercell_coord[p2s[i]]
        vectors[i, j, :] = R
        shortest_vectors = find_shortest(R, Lvectors, 0.001)
        vectors_L.append(shortest_vectors)
        multi[i, j] = len(shortest_vectors)
        index[i, j] = counter
        counter += 1

# load the J_prime_st_R
# shape is (2, 2, 18, 3)
# s, t, num_atom, xyz components
# Eq. (35)
# real: Jxz, imag: Jyz
J_prime_st_R = np.load('force_off_diagonal_6x6/J_prime_st_R.npy')
print('shape of J_prime_st_R', J_prime_st_R.shape)

# lattice vectors
a = 7.0017278277323989
a1 = np.array([a, 0.0, 0.0])
a2 = np.array([-a / 2.0, np.sqrt(3) * a / 2.0, 0.0])
a3 = np.array([0.0, 0.0, 23.0])
b1, b2, b3 = get_reciprocal([a1, a2, a3])
S = 3.0 / 2

# load the q grids
q_points = np.load(f'{phonon_folder}/q_points_{kpath}.npy')
nq = q_points.shape[0]
# print('q vectors: \n', q_points)

# phonon eigenvectors calculated in phonon folder
# we have total 24 phonon modes
# here the eignvectors.npy contain the eigenvectors of a q-grid
# (nq, 24, 8, 3, 2)
# nq is the number of q vectors
# 24 is the number of phonon bands
# 8 is the number of atoms of CrI3 in one unit cell
# 3 is the x, y, z Cartesian coordinates
# 2 is the real and imaginary part
# e_q contains real and imaginary parts
phonon_energies = np.load(f'{phonon_folder}/eigenvalues_{kpath}.npy')
e_q = np.load(f'{phonon_folder}/eigenvectors_{kpath}.npy')
print('shape of phonon energies', phonon_energies.shape)
print('shape of phonon eigenvectors', e_q.shape)

# magnon energies and eigenvectors in magnon folder
# here the eignvectors.npy contain the eigenvectors of a q-grid
# f_q is sorted in columns for acoustic and optic branches
magnon_energies = np.load(f'{magnon_folder}/eigenvalues_{kpath}_{label}.npy')
f_q = np.load(f'{magnon_folder}/eigenvectors_{kpath}_{label}.npy')
print('shape of magnon eigenvectors', f_q.shape)

# calculate linear magnon-phonon coupling
# (nq, 2, 24), 9 q-vectors, 2 magnon modes and 24 phonon modes
lambda_q = np.zeros((nq, 2, 24), dtype=complex)
# for each q vector, calculate the coupling constant
for iq in range(nq):
    # Fourier transform of J'
    # s, t, xyz components
    # (2, 2, 3) s, t, x/y/z Cartesian
    # s, t could be 0 or 1 for the two Cr atoms in one unit cell
    J_prime_st_q = np.zeros((2, 2, 3), dtype=complex)
    q1 = q_points[iq][0]
    q2 = q_points[iq][1]
    # convert q vector from direct to Cartesian
    q = q1 * b1 + q2 * b2
    for i in range(primitive_num):
        for j in range(supercell_num):
            # supercell to primitive
            k = s2p[j]
            ind = index[i, j]
            mul = multi[i, j]
            # sum over phase
            phase = 0
            for vec in vectors_L[ind]:
                x1, x2 = vec[0], vec[1]
                R = x1 * a1 * 6 + x2 * a2 * 6
                phase += np.exp(1j * np.dot(q, R)) / mul
            J_prime_st_q[i, k, :] += J_prime_st_R[i, k, j, :] * phase
    # get lambda
    for mu in range(2):
        for nu in range(24):
            # calculate phonon amplitude
            hbar = 1.054e-34  # J.s
            e = 1.602e-19
            AMU = 1.66e-27  # kg
            m = 51.99 * AMU
            energy = phonon_energies[iq, nu]
            omega = energy * 1e-3 * e / hbar
            # convert phonon amplitude to angstrom
            amp = np.sqrt(hbar / 2 / m / omega) / 1e-10
            lambda_q[iq, mu, nu] = S * np.sqrt(2 * S) * amp * \
                                   (f_q[iq, 0, mu].conj() * np.dot(J_prime_st_q[0, 0, :],
                                                                   (e_q[iq, nu, 0, :, 0] + 1j * e_q[iq, nu, 0, :, 1])) +
                                    f_q[iq, 1, mu].conj() * np.dot(J_prime_st_q[1, 0, :],
                                                                   (e_q[iq, nu, 0, :, 0] + 1j * e_q[iq, nu, 0, :, 1])) +
                                    f_q[iq, 1, mu].conj() * np.dot(J_prime_st_q[1, 1, :],
                                                                   (e_q[iq, nu, 1, :, 0] + 1j * e_q[iq, nu, 1, :, 1])) +
                                    f_q[iq, 0, mu].conj() * np.dot(J_prime_st_q[0, 1, :],
                                                                   (e_q[iq, nu, 1, :, 0] + 1j * e_q[iq, nu, 1, :, 1])))
print('shape of lambda_q: ', lambda_q.shape)
lambda_q = np.nan_to_num(lambda_q, nan=0.0, posinf=0.0, neginf=0.0)
np.save(f'lambda_{kpath}_{label}.npy', lambda_q)