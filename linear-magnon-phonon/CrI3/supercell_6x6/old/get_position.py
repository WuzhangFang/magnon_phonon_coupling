import numpy as np

# read the POSCAR and save the coordinates 

# Cartesian
file_input = open('POSCAR-C.vasp')
data = file_input.readlines()
file_input.close()

# store the type and number of each element
elements_label = data[5].split()
num_elements = np.fromstring(data[6], dtype=int, sep=' ')
total_num = np.sum(num_elements)
elements = []

coordinates = []
for i in range(total_num):
    c = np.fromstring(data[8+i], dtype=float, sep=' ')
    coordinates.append(c)

coordinates = np.array(coordinates)

print(coordinates.shape)
print(coordinates)
np.save('coordinates_Cartesian.npy', coordinates)

# Direct
file_input = open('POSCAR-D.vasp')
data = file_input.readlines()
file_input.close()

# store the type and number of each element
elements_label = data[5].split()
num_elements = np.fromstring(data[6], dtype=int, sep=' ')
total_num = np.sum(num_elements)
elements = []

coordinates = []
for i in range(total_num):
    c = np.fromstring(data[8+i], dtype=float, sep=' ')
    coordinates.append(c)

coordinates = np.array(coordinates)

print(coordinates.shape)
print(coordinates)
np.save('coordinates_direct.npy', coordinates)