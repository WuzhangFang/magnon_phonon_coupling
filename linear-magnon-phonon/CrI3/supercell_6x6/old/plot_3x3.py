import numpy as np
import matplotlib.pyplot as plt

# grid 3x3:exact for Fourier transform in a 3x3 supercell
system = 'q3x3'
label = 'DFT'
lambda_q_3 = np.load(f'lambda_{system}_{label}.npy')
print('shape of lambda_q: ', lambda_q_3.shape)
phonon_folder = '../data_phonon'
q_3 = np.load(f'{phonon_folder}/q_points_{system}.npy')
print('shape of q_points: ', q_3.shape)

# plotting
fig, axs = plt.subplots(3, 3, figsize=(16,9), gridspec_kw={'hspace': 0.0, 'wspace': 0.0})
for i in range(3):
    for j in range(3):
        iq = i * 3 + j
        axs[i,j].plot(np.abs(lambda_q_3[iq,0,:]), '.-', label='Magnon acoustic mode')
        axs[i,j].plot(np.abs(lambda_q_3[iq,1,:]), '.-', label='Magnon optic mode')
        axs[i,j].set_ylim(0,0.6)
        axs[i,j].set_xticklabels([])
        axs[i,j].set_yticklabels([])
        axs[i,j].text(8, 0.25, f'q: ({q_3[iq][0]:.2f}, {q_3[iq][1]:.2f}, 0)', fontsize=12)

axs[0,0].set_ylabel(r'$|\lambda|$ (meV)')
axs[1,0].set_ylabel(r'$|\lambda|$ (meV)')
axs[2,0].set_ylabel(r'$|\lambda|$ (meV)')
yticks = [float(f'{y:.2f}') for y in np.arange(0.00, 0.60, 0.10)]

xticks = np.arange(0, 24, 2)
axs[0,0].set_yticks(yticks)
axs[1,0].set_yticks(yticks)
axs[2,0].set_yticks(yticks)
axs[0,0].set_yticklabels(yticks)
axs[1,0].set_yticklabels(yticks)
axs[2,0].set_yticklabels(yticks)

axs[2,0].set_xticks(xticks)
axs[2,0].set_xlabel('Phonon mode #')
axs[2,0].set_xticklabels(xticks)
axs[2,1].set_xticks(xticks)
axs[2,1].set_xlabel('Phonon mode #')
axs[2,1].set_xticklabels(xticks)
axs[2,2].set_xticks(xticks)
axs[2,2].set_xlabel('Phonon mode #')
axs[2,2].set_xticklabels(xticks)
axs[0,0].legend(frameon=False)

plt.savefig('coupling_3x3.png', transparent=True, dpi=300, bbox_inches='tight')
plt.show()

