# add the magnon-phonon coupling terms
# use the parameters from https://journals.aps.org/prb/abstract/10.1103/PhysRevB.107.214452
# implement eq. (10)
# TODO need to use more kpoints in DFT to have a better interpolation
import numpy as np
import matplotlib.pyplot as plt
from helper import model, phonon_interpolate

# CrI3
# lattice vectors
a1 = np.array([1.0, 0.0, 0.0])
a2 = np.array([-1/2.0, np.sqrt(3)/2.0, 0.0])
a3 = np.array([0.0, 0.0, 1.0])
S = 3/2

# unit: meV 
# parameters from https://journals.aps.org/prb/abstract/10.1103/PhysRevB.106.214424
# notice there is 1/2 as a prefactor in spin Hamiltonian
J1, J2, J3 = (2.94, 0.62, -0.16)
z1, z2, z3 = (3, 6, 3)

cri3 = model([a1, a2, a3], [J1, J2, J3], [z1, z2, z3], S)
# reciprocal vectors
b1, b2, b3 = cri3.get_reciprocal()

# plot band structure
# direct
G = [0.0, 0.0, 0.0]
K = [0.333333, 0.333333, 0.0]
M = [0.5, 0.0, 0.0]
kpoints = np.array([G, K, M, G])

def H_k(k, J_list, z_list, S):
    """
    1. magnon
    up to third-nearest neighbor Hamiltonian
    for each k=[k1, k2], return the eigenvalue
    the basis set is a+, b+ (row) a, b (column)
    2. phonon
    use interpolation to get the band energy at arbitary k point 
    from DFT calculation using VASP and phonopy
    3. magnon-phonon coupling
    """
    # number of bands
    nb_ph = 24
    nb_mag = 2
    # Direct
    k1, k2 = k[0], k[1]
    # Cartesian
    k_C = k1 * b1 + k2 * b2
    kx, ky = k_C[0], k_C[1]
    nb = nb_ph + nb_mag
    H_k = np.zeros((nb, nb), dtype=complex)
    # magnon
    # first two bands
    J1, J2, J3 = J_list
    z1, z2, z3 = z_list
    Ak = z1 * J1 + z2 * J2 + z3 * J3 - 2 * J2 * (np.cos(kx) + 2*np.cos(kx/2)*np.cos(np.sqrt(3)*ky/2))
    Bk = -J1 * (np.exp(1j*np.sqrt(3)*ky/3) + 2*np.cos(kx/2)*np.exp(-1j*np.sqrt(3)*ky/6)) \
         -J3 * (np.exp(-1j*2*np.sqrt(3)*ky/3) + 2*np.cos(kx)*np.exp(1j*np.sqrt(3)*ky/3)) 
    H_k[0,0] = S * Ak
    H_k[0,1] = S * Bk
    H_k[1,0] = S * np.conjugate(Bk)
    H_k[1,1] = S * Ak
    # phonon 
    # for each phonon band add in the diagonal
    for ib in range(nb_ph):
        H_k[ib+2,ib+2] = phonon_interpolate([k1, k2], ib)
    # magnon-phonon coulping
    # diagonalize
    eigenvalues, eigenvectors = np.linalg.eig(H_k)
    sorted_index = np.argsort(eigenvalues)
    sorted_eigenvalues = eigenvalues[sorted_index]
    sorted_eigenvectors = eigenvectors[:, sorted_index]
    return sorted_eigenvalues

X, E, d = cri3.band(kpoints, H_k, Nb=26, Nk=51)
print(d)
plt.plot(X, E, color='red')

# format
plt.xlim(d[0],d[-1])
plt.ylim(0,)
plt.axvline(d[1], c='black', lw=0.5)
plt.axvline(d[2], c='black', lw=0.5)
xlabels = [r'$\Gamma$', '$K$', '$M$', r'$\Gamma$']
plt.xticks(d, xlabels)
plt.ylabel('Energy (meV)')
plt.savefig('CrI3_magnon_phonon.png', dpi=300, bbox_inches='tight')
plt.show()