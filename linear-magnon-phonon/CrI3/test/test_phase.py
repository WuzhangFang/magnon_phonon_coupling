import numpy as np
import matplotlib.pyplot as plt
from helper import get_reciprocal, find_shortest
import itertools

np.set_printoptions(precision=6)

def main():
    phonon_folder = '../phonon_q_27x27'

    # 18 Cr atoms in a supercell
    primitive_num = 2
    supercell_num = 18

    # supercell to primitive cell
    s2p = np.array([0, 1] * 3 * 3, dtype=int)
    p2s = np.array([0, 1], dtype=int)

    # load the coordinates
    supercell_coord = np.load('../supercell_3x3/coordinates_direct.npy')[:supercell_num]
    # generate the Lvectors
    values = [-1, 0, 1]
    # Use itertools.product to generate all combinations
    combinations = list(itertools.product(values, repeat=3))
    Lvectors = np.array(combinations)

    multi = np.zeros((primitive_num, supercell_num), dtype=int)
    index = np.zeros((primitive_num, supercell_num), dtype=int)
    # relative distance between atomi and atomj in the supercell
    vectors = np.zeros((primitive_num, supercell_num, 3))
    # add supercell vectors
    vectors_L = []
    counter = 0

    for i in range(primitive_num):
        for j in range(supercell_num):
            # primitive to supercell
            # store the vector
            R = supercell_coord[j] - supercell_coord[p2s[i]]
            vectors[i, j, :] = R
            shortest_vectors = find_shortest(R, Lvectors, 0.001)
            vectors_L.append(shortest_vectors)
            multi[i, j] = len(shortest_vectors)
            index[i, j] = counter
            counter += 1
    #print('vectors', vectors)
    #print('vectors_L', vectors_L)
    print('multiplicity: \n', multi)
    # load the J_prime_st_R
    # shape is (2, 2, 18, 3)
    # s, t, num_atom, xyz components
    # Eq. (35)
    # real: Jxz, imag: Jyz
    J_prime_st_R = np.load('../supercell_3x3/force_off_diagonal_3x3/J_prime_st_R.npy')

    # lattice vectors
    a = 7.0017278277323989
    a1 = np.array([a, 0.0, 0.0])
    a2 = np.array([-a / 2.0, np.sqrt(3) * a / 2.0, 0.0])
    a3 = np.array([0.0, 0.0, 23.0])
    b1, b2, b3 = get_reciprocal([a1, a2, a3])

    # load the q grids
    q_points = np.load(f'{phonon_folder}/q_points.npy')

    # test parameters
    iq = 2
    i = 0
    j = 17

    # vector
    vec = vectors[i, j]
    print('vector: \n', vec)
    # vector_L
    vec_L = vectors_L[index[i, j]]
    print('vector_L: \n', vec_L)

    # q vector
    print('q vectors: \n', q_points[iq])
    q1 = q_points[iq, 0]
    q2 = q_points[iq, 1]
    # convert q vector from direct to Cartesian
    q = q1 * b1 + q2 * b2

    # Example of using the functions (this part can be modified based on how you want to use them)
    print(old_phase(i, j, q, s2p, vectors, a1, a2, J_prime_st_R))
    print(new_phase(i, j, q, s2p, index, multi, vectors_L, a1, a2, J_prime_st_R))


def old_phase(i, j, q, s2p, vectors, a1, a2, J_prime_st_R):
    """
    new version
    includes the supercell lattice vectors
    i: index in primitive cell
    j: index in supercell
    """
    # supercell to primitive
    k = s2p[j]
    vec = vectors[i, j]
    x1, x2 = vec[0], vec[1]
    R = x1 * a1 * 3 + x2 * a2 * 3
    phase = np.exp(1j * np.dot(q, R))
    print('----old phase----')
    print(phase)
    J_prime_st_q = J_prime_st_R[i, k, j, :] * phase
    return np.abs(J_prime_st_q)


def new_phase(i, j, q, s2p, index, multi, vectors_L, a1, a2, J_prime_st_R):
    """
    new version
    includes the supercell lattice vectors
    i: index in primitive cell
    j: index in supercell
    """
    # supercell to primitive
    k = s2p[j]
    ind = index[i, j]
    mul = multi[i, j]
    # sum over phase
    phase = 0
    print('----new phase----')
    print('multipliticy:', mul)
    for vec in vectors_L[ind]:
        x1, x2 = vec[0], vec[1]
        R = x1 * a1 * 3 + x2 * a2 * 3
        phase += np.exp(1j * np.dot(q, R)) / mul
    print('phase: ', phase)
    J_prime_st_q = J_prime_st_R[i, k, j, :] * phase

    return np.abs(J_prime_st_q)

if __name__ == "__main__":
    main()
