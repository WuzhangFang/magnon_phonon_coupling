import numpy as np
import matplotlib.pyplot as plt
from helper import model

def main():
    # load q vectors from phonon calculations
    kpath = 'q3x3'
    label = 'DFT_5'

    q_points = np.load(f'../data_phonon/q_points_{kpath}.npy')
    nq = q_points.shape[0]
    print(nq)
    print(q_points)
    frequencies = np.zeros((nq, 1))

    S = 3 / 2
    J_list = 2 * np.array([8.6, 2.6, -2.3, 0.706]) / S**2

    z_list = (6, 6, 6)

    # go over q vectors
    for iq in range(nq):
        q = q_points[iq]
        freq = H_k(q, J_list, z_list)
        frequencies[iq, :] = freq
    print(frequencies.shape)
    print(frequencies)
    np.save(f'eigenvalues_{kpath}_{label}.npy', frequencies)

def H_k(k, J_list, z_list):
    # CrI3
    # lattice vectors
    a1 = np.array([1.0, 0.0, 0.0])
    a2 = np.array([-1/2.0, np.sqrt(3)/2.0, 0.0])
    a3 = np.array([0.0, 0.0, 1.0])
    S = 3/2

    # unit: meV 
    # parameters from https://journals.aps.org/prb/abstract/10.1103/PhysRevB.106.214424
    # notice there is 1/2 as a prefactor in spin Hamiltonian
    J1, J2, J3 = (J_list[0], J_list[1], J_list[2])
    z1, z2, z3 = (z_list[0], z_list[1], z_list[2])
    Az = J_list[3]

    cri3 = model([a1, a2, a3], [J1, J2, J3], [z1, z2, z3], S)
    # reciprocal vectors
    b1, b2, b3 = cri3.get_reciprocal()

    k1 = k[0]
    k2 = k[1]
    # Cartesian
    k_C = k1 * b1 + k2 * b2
    kx, ky = k_C[0], k_C[1]
    Ak = Az + z1 * J1 + z2 * J2 + z3 * J3 - J1 * 2 * (np.cos(kx) + 2*np.cos(kx/2)*np.cos(np.sqrt(3)*ky/2)) \
    - J2 * 2 * (np.cos(np.sqrt(3)*ky) + 2*np.cos(3*kx/2)*np.cos(np.sqrt(3)*ky/2)) \
    - J3 * 2 * (np.cos(2*kx) + 2*np.cos(kx)*np.cos(np.sqrt(3)*ky))

    return S * Ak

if __name__ == "__main__":
    main()
