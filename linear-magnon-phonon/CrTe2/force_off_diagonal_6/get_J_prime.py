import numpy as np
from helper import get_derivative

# 9 Cr and 18 Te atoms in a 3x3x1 supercell
num_primitive = 3
num_atom = 27

# supercell to primitive cell
s2p = [0] * 9 + [1] * 9 + [2] * 9
s2p = np.array(s2p, dtype=int)

# get the J' for Cr atom in a 3x3 supercell
# shape: Cr, num_atom, x/y/z
J_prime_real = np.zeros((1, num_atom, 3))
J_prime_imag = np.zeros((1, num_atom, 3))

# sum over six nearest neighbors
for pair in ['1-2', '1-3', '1-4', '1-5', '1-7', '1-9', ]:
    J_prime_real[0,:,:] += get_derivative(f"./X-{pair}.dat", num_atom)
    J_prime_imag[0,:,:] += get_derivative(f"./Y-{pair}.dat", num_atom)

# reformulate J'
# s:0, t:0-2, num_atom, xyz components
# Eq. (35)
# real: Jxz, imag: Jyz
J_prime_st_R = np.zeros((1, num_primitive, num_atom, 3), dtype=complex)
for i in range(num_atom):
    pos = s2p[i]
    J_prime_st_R[0,pos,i,:] = J_prime_real[0,i,:] - 1j*J_prime_imag[0,i,:]

print(J_prime_st_R.shape)
np.save('J_prime_st_R.npy', J_prime_st_R)
