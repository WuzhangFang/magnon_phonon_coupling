import numpy as np
from helper import get_reciprocal, find_shortest, read_poscar
import itertools
import sys
np.set_printoptions(precision=4, suppress=True)

# phonon frequencies and eigenvectors are calculated in data_phonon
# magnon frequencies and eigenvectors are calculated in data_magnon
# outline: (1) Fourier transform of J_prime_st_R
#          (2) get the couping constant using J_prime_q and eigenvectors of magnon and phonon
# 07/11/2024: modify the phase factor to include the supercell size
# 09/23/2024: includes phonon modes from all the atoms including Iodine atoms
# 10/02/2024: modify for CrTe2. The big difference is that there is only on magnon band so no
#             need to use the magnon eigenvector

strain = sys.argv[1]
kpath = sys.argv[2]
label = f'DFT_{strain}'
magnon_folder = './data_magnon'
phonon_folder = './data_phonon'

# 9 Cr and 18 Te atoms in a 3x3x1 supercell
primitive_num = 1
supercell_num = 27

# supercell to primitive cell
s2p = [0] * 9 + [1] * 9 + [2] * 9
s2p = np.array(s2p, dtype=int)

# primitive to supercell
p2s = [0, 9, 18]

# load the coordinates
_, _, _, supercell_coord = read_poscar(f'SPOSCAR-{strain}')
print(supercell_coord.shape)

# load the q grids
q_points = np.load(f'{phonon_folder}/q_points_{kpath}.npy')
nq = q_points.shape[0]

# load the J_prime_st_R
# shape is (1, 3, 27, 3)
# s, t, num_atom, xyz components
# Eq. (35)
# real: Jxz, imag: Jyz
J_prime_st_R = np.load(f'force_off_diagonal_{strain}/J_prime_st_R.npy')
print('shape of J_prime_st_R', J_prime_st_R.shape)

# phonon eigenvectors calculated in phonon folder
# here the eignvectors.npy contain the eigenvectors of a q-grid
# (nq, 9, 3, 3, 2)
# nq is the number of q vectors
# 9 is the number of phonon bands
# 3 is the number of atoms of CrI3 in one unit cell
# 3 is the x, y, z Cartesian coordinates
# 2 is the real and imaginary part
# e_q contains real and imaginary parts
phonon_energies = np.load(f'{phonon_folder}/eigenvalues_{kpath}.npy')
e_q = np.load(f'{phonon_folder}/eigenvectors_{kpath}.npy')
print('shape of phonon energies', phonon_energies.shape)
print('shape of phonon eigenvectors', e_q.shape)

# magnon energies in magnon folder
magnon_energies = np.load(f'{magnon_folder}/eigenvalues_{kpath}_{label}.npy')

# generate the Lvectors
values = [-1, 0, 1]
# Use itertools.product to generate all combinations
combinations = list(itertools.product(values, repeat=3))
Lvectors = np.array(combinations)

multi = np.zeros((primitive_num, supercell_num), dtype=int)
index = np.zeros((primitive_num, supercell_num), dtype=int)
# relative distance between atomi and atomj in the supercell
vectors = np.zeros((primitive_num, supercell_num, 3))
# add supercell vectors
vectors_L = []
counter = 0

for i in range(primitive_num):
    for j in range(supercell_num):
        # primitive to supercell
        # store the vector
        R = supercell_coord[j] - supercell_coord[p2s[i]]
        vectors[i, j, :] = R
        shortest_vectors = find_shortest(R, Lvectors, 0.001)
        vectors_L.append(shortest_vectors)
        multi[i, j] = len(shortest_vectors)
        index[i, j] = counter
        counter += 1

# lattice vectors
a1, a2, a3, _ = read_poscar(f'POSCAR-{strain}')
b1, b2, b3 = get_reciprocal([a1, a2, a3])
S = 3.0 / 2

# calculate linear magnon-phonon coupling
lambda_q = np.zeros((nq, 1, 9), dtype=complex)
# mass list, one Cr and two Te atoms
mass = [51.996100] * 1 + [127.600000] * 2
# for each q vector, calculate the coupling constant
for iq in range(nq):
    # Fourier transform of J'
    # s, t, xyz components
    # (1, 3, 3) s, t, x/y/z Cartesian
    # s:0 for Cr, t:0-2 all the atoms in one unit cell
    J_prime_st_q = np.zeros((primitive_num, 3, 3), dtype=complex)
    q1 = q_points[iq][0]
    q2 = q_points[iq][1]
    # convert q vector from direct to Cartesian
    q = q1 * b1 + q2 * b2
    for i in range(primitive_num):
        for j in range(supercell_num):
            # supercell to primitive
            k = s2p[j]
            ind = index[i, j]
            mul = multi[i, j]
            # sum over phase
            phase = 0
            for vec in vectors_L[ind]:
                x1, x2 = vec[0], vec[1]
                R = x1 * a1 * 3 + x2 * a2 * 3
                phase += np.exp(1j * np.dot(q, R)) / mul
            J_prime_st_q[i,k,:] += J_prime_st_R[i,k,j,:] * phase
    # get lambda
    for mu in range(1):
        for nu in range(9):
            # calculate phonon amplitude
            hbar = 1.054e-34  # J.s
            e = 1.602e-19
            AMU = 1.66e-27  # kg
            energy = phonon_energies[iq, nu]
            omega = energy * 1e-3 * e / hbar
            # calculate coupling
            for t in range(3):
                # convert phonon amplitude to angstrom
                amp = np.sqrt(hbar / 2 / (mass[t]*AMU) / omega) / 1e-10                
                lambda_q[iq, mu, nu] += S * np.sqrt(2 * S) * amp * np.dot(J_prime_st_q[0,t,:], (e_q[iq,nu,t,:, 0] + 1j * e_q[iq,nu,t,:,1]))
 
print('shape of lambda_q: ', lambda_q.shape)
lambda_q = np.nan_to_num(lambda_q, nan=0.0, posinf=0.0, neginf=0.0)
np.save(f'lambda/lambda_{kpath}_{label}.npy', lambda_q)
