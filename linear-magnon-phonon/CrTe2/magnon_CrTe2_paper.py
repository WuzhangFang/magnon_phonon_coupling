import numpy as np
import matplotlib.pyplot as plt
from helper import model

# CrTe2
# lattice vectors
a1 = np.array([1.0, 0.0, 0.0])
a2 = np.array([-1/2.0, np.sqrt(3)/2.0, 0.0])
a3 = np.array([0.0, 0.0, 1.0])

S = 3/2
# unit: meV
# PBE calculations with U=2 eV
# we put a 1/2 in the spin Hamiltonian
# last element is the anisotropy constant, positive means easy axis
#J0 = -1 * np.array([-19.3, -4.2, 0.5, 0.0]) / S**2
#J1 = -1 * np.array([-20.2, -1.5, 1.6, -0.706*2]) / S**2
#J2 = -1 * np.array([-22.7, -0.7, 3.0, -1.927*2]) / S**2
J0 = 2 * np.array([6.9, 2.2, -1.8, 0.0]) / S**2
J1 = 2 * np.array([8.6, 2.6, -2.3, 0.706]) / S**2
J2 = 2 * np.array([11.6, 1.4, -1.3, 1.927]) / S**2

def H_k(k, J_list, z_list, S):
    """
    for magnon
    up to third-nearest neighbor
    for each k=[kx, ky], return the eigenvalue
    """
    # number of bands
    k1 = k[0]
    k2 = k[1]
    # Cartesian
    k_C = k1 * b1 + k2 * b2
    kx, ky = k_C[0], k_C[1]
    J1, J2, J3, Az = J_list[0], J_list[1], J_list[2], J_list[3]
    z1, z2, z3 = z_list
    Ak = Az + z1 * J1 + z2 * J2 + z3 * J3 - J1 * 2 * (np.cos(kx) + 2*np.cos(kx/2)*np.cos(np.sqrt(3)*ky/2)) \
    - J2 * 2 * (np.cos(np.sqrt(3)*ky) + 2*np.cos(3*kx/2)*np.cos(np.sqrt(3)*ky/2)) \
    - J3 * 2 * (np.cos(2*kx) + 2*np.cos(kx)*np.cos(np.sqrt(3)*ky))
    return S * Ak

# plot band structure
G = [0.0, 0.0, 0.0]
K = [0.333333, 0.333333, 0.0]
M = [0.5, 0.0, 0.0]
kpoints = np.array([G, K, M, G])

# 1st, 2nd, 3rd nearest neighbors
z1, z2, z3 = (6, 6, 6)

cri30 = model([a1, a2, a3], J0, [z1, z2, z3], S)
cri31 = model([a1, a2, a3], J1, [z1, z2, z3], S)
cri32 = model([a1, a2, a3], J2, [z1, z2, z3], S)
# reciprocal vectors
b1, b2, b3 = cri30.get_reciprocal()


X0, E0, d0 = cri30.band(kpoints, H_k, Nb=1, Nk=101)
X1, E1, d1 = cri31.band(kpoints, H_k, Nb=1, Nk=101)
X2, E2, d2 = cri32.band(kpoints, H_k, Nb=1, Nk=101)

# format
fs = 12  # Font size
lw = 1   # Line width
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4,3))
ax.plot(X0, E0, lw=lw, color='black', label='4%')
ax.plot(X1, E1, lw=lw, color='blue', label='5%')
ax.plot(X2, E2, lw=lw, color='red', label='6%')
ax.set_xlim(d0[0],d0[-1])
ax.set_ylim(0,)
xlabels = [r'$\Gamma$', 'K', 'M', r'$\Gamma$']
ax.tick_params(labelsize=fs-2)
ax.set_xticks(d0, xlabels)
ax.set_ylabel('Energy (meV)', fontsize=fs)
for x in d0:
    ax.axvline(x, lw=0.5, c='black')
ax.legend(fontsize=fs-2, facecolor='none', edgecolor='none', loc=(0.755,0.7))

# width and height are relative to the main plot
ax_inset = fig.add_axes([0.4, 0.2, 0.18, 0.18])
ax_inset.plot(X0, E0, lw=lw, color='black', label='4%')
ax_inset.plot(X1, E1, lw=lw, color='blue', label='5%')
ax_inset.plot(X2, E2, lw=lw, color='red', label='6%')
ax_inset.tick_params(labelsize=fs-4)
d0 = [d0[0]]
xlabels = [r'$\Gamma$']
ax_inset.set_xticks(d0, xlabels)
ax_inset.set_xlim(0,0.1)
ax_inset.set_ylim(0,5)
ax_inset.set_xticks(d0, xlabels)
ax_inset.set_yticks(np.arange(0,6,2.5))

plt.savefig('strain_magnon.pdf', dpi=300, bbox_inches='tight')
plt.savefig('strain_magnon.png', dpi=300, bbox_inches='tight')
#plt.show()