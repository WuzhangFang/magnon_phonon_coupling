import numpy as np
from helper import read_poscar
# Based on the distance, find the neighbors

class Atom:
    """
    save the index and coordinates
    """
    def __init__(self, index: int, cord: np.ndarray) -> None:
        self.index = index
        self.coordinate = cord
    def get_distance(self, atom2):
        distance = np.linalg.norm(self.coordinate - atom2.coordinate)
        return distance

# read the coordinates in Cartesian
a1, a2, a3, supercell_coord = read_poscar('SPOSCAR-C.vasp')

supercell = []
for i in range(supercell_coord.shape[0]):
    coord = supercell_coord[i,:]
    supercell.append(Atom(i, coord))

neighbors = [[0, 0], [1, 0], [-1, 0], [0, 1], [0, -1], [1, 1], [-1, 1], [-1, -1], [1, -1]]
supercell_neighbors = []
for n in neighbors:
    i = n[0]
    j = n[1]
    for atom in supercell:
        coord = atom.coordinate + i * a1 + j * a2
        supercell_neighbors.append(Atom(atom.index, coord))

def get_neighbor_index(supercell, supercell_neighbors, atom1_i, d_nn):
    """
    get the index of neighbors
    """
    atom1 = supercell[atom1_i]
    index_list = []
    for atom2 in supercell_neighbors:
        distance = atom1.get_distance(atom2)
        if abs(distance - d_nn) < 0.01:
            index_list.append(atom2.index)
    index_list = np.array(index_list, dtype=int) + 1
    # here add 1 to make the index starting from 1 rather than 0
    return np.sort(index_list)

# Cr1
print(get_neighbor_index(supercell, supercell_neighbors, 0, 3.91168))
print(get_neighbor_index(supercell, supercell_neighbors, 0, 6.77523))