import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.colors as mcolors

# get the gap openning at K by diaognalizing the Hamiltonian

kpath = 'GKMG'
label = 'DFT_3.91'
magnon_folder = './data_magnon'
phonon_folder = './data_phonon'

# load eigenvalues and eigenvectors of phonon
phonon_energies = np.load(f'{phonon_folder}/eigenvalues_{kpath}.npy')
print('shape of phonon energies', phonon_energies.shape)

# load eigenvalues and eigenvectors of magnon
magnon_energies = np.load(f'{magnon_folder}/eigenvalues_{kpath}_{label}.npy')
print('shape of magnon energies', magnon_energies.shape)

# load coupling constants
lambda_q = np.load(f'lambda/lambda_{kpath}_{label}.npy')
print('shape of couling constants', lambda_q.shape)

# load q points
q_points = np.load(f'{phonon_folder}/q_points_{kpath}.npy')

# number of bands
nq = phonon_energies.shape[0]
nb_ph = phonon_energies.shape[1]
nb_mag = magnon_energies.shape[1]
nb = nb_ph + nb_mag

magnon_weight = np.zeros((nq,nb))
eigenvalues = np.zeros((nq,nb))

for iq in range(nq):
    # construct the Hamiltonian
    H_k = np.zeros((nb, nb), dtype=complex)
    # magnon
    for ib in range(nb_mag):
        H_k[ib, ib] = magnon_energies[iq, ib]
    # phonon
    for ib in range(nb_ph):
        H_k[ib+nb_mag,ib+nb_mag] = phonon_energies[iq, ib]
    # coupling
    for i in range(nb_mag):
        for j in range(nb_ph):
            H_k[i,j+nb_mag] = lambda_q[iq,i,j]
            H_k[j+nb_mag,i] = lambda_q[iq,i,j].conj()

    eig, eigv = np.linalg.eig(H_k)
    # calculate magnon weight
    weight = np.linalg.norm(eigv[0:nb_mag,:], axis=0)
    eigenvalues[iq,:] = np.real(eig)
    magnon_weight[iq, :] = weight

print('shape of eigenvalues: ', eigenvalues.shape)
print('shape of magnon weight: ', magnon_weight.shape)

# plotting
index_K = 501
xx = np.arange(index_K*3)
xticks = [0, index_K, index_K*2, index_K*3]
xticklabels = [r'$\Gamma$', 'K', 'M', r'$\Gamma$']
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(4,3))
fs = 14  # Font size

# Normalize the color range from 0 to 1
norm = mcolors.Normalize(vmin=0, vmax=1)
# Define custom colormap from blue to red
colors = [(0, 0, 1), (1, 0, 0)]  # Blue to Red
n_bins = 100  # Discretize the interpolation into bins
cmap_name = 'blue_red'
# Create the colormap
cm = LinearSegmentedColormap.from_list(cmap_name, colors, N=n_bins)

for ib in range(nb):
    sc = ax.scatter(xx, eigenvalues[:,ib], s=0.5, c=magnon_weight[:,ib], cmap=cm, alpha=0.5, norm=norm)

cb = fig.colorbar(sc, ax=ax)
cb.set_label("Magnon weight", rotation=270, labelpad=25)

ax.set_xlim(0, index_K*3)
ax.set_ylim(10,40)
ax.set_ylabel('Energy (meV)')
ax.set_xticks(xticks, xticklabels)
for xtick in xticks:
    ax.axvline(x=xtick, color='black', linewidth=0.5)
plt.tight_layout()
plt.savefig(f'figures/gap_{label}.png', transparent=True, dpi=300, bbox_inches='tight')
plt.savefig(f'figures/gap_{label}.pdf', transparent=True, dpi=300, bbox_inches='tight')
plt.show()