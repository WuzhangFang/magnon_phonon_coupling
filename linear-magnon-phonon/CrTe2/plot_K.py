import numpy as np
import matplotlib.pyplot as plt

# grid 3x3: exact for Fourier transform in a 3x3 supercell
system = 'q3x3'
label = 'DFT_3.91'
lambda_q_3 = np.load(f'lambda/lambda_{system}_{label}.npy')
print('shape of lambda_q: ', lambda_q_3.shape)
phonon_folder = './data_phonon'
q_3 = np.load(f'{phonon_folder}/q_points_{system}.npy')
print('shape of q_points: ', q_3.shape)

# plotting
fig, axs = plt.subplots(1, 1, figsize=(8, 4.5))
iq = 4  # plot the K point

# Convert line plot to bar plot
axs.bar(np.arange(len(lambda_q_3[iq, 0, :]))+1, np.abs(lambda_q_3[iq, 0, :]))

xticks = np.arange(1, 9, 1)
yticks = [float(f'{y:.2f}') for y in np.arange(0.00, 1.5, 0.25)]

axs.set_xticks(xticks)
axs.set_yticks(yticks)
axs.set_yticklabels(yticks)
axs.set_xlabel('Phonon mode #')
axs.set_ylabel(r'$|\lambda|$ (meV)')

#axs.legend(frameon=False)

plt.savefig(f'figures/coupling_K_{label}.png', transparent=True, dpi=300, bbox_inches='tight')
plt.show()

