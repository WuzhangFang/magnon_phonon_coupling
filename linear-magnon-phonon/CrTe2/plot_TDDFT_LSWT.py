import numpy as np
import matplotlib.pyplot as plt
from helper import model

# CrTe2
# lattice vectors
a1 = np.array([1.0, 0.0, 0.0])
a2 = np.array([-1/2.0, np.sqrt(3)/2.0, 0.0])
a3 = np.array([0.0, 0.0, 1.0])

S = 3/2
# unit: meV parameters from LDA calculation
# lattice constant 3.91, 6%
# we put a 1/2 in the spin Hamiltonian
# last element is the anisotropy constant, positive means easy axis
J = np.array([5.4, 3.4, 1.7, 2.5])
J_scaled = J * 2 / S**2
print(J_scaled)

# 1st, 2nd, 3rd nearest neighbors
z1, z2, z3 = (6, 6, 6)

cri3 = model([a1, a2, a3], J_scaled, [z1, z2, z3], S)
# reciprocal vectors
b1, b2, b3 = cri3.get_reciprocal()

# plot band structure
G = [0.0, 0.0, 0.0]
K = [0.333333, 0.333333, 0.0]
M = [0.5, 0.0, 0.0]
kpoints = np.array([G, M])

def H_k(k, J_list, z_list, S):
    """
    for magnon
    up to third-nearest neighbor
    for each k=[kx, ky], return the eigenvalue
    """
    # number of bands
    k1 = k[0]
    k2 = k[1]
    # Cartesian
    k_C = k1 * b1 + k2 * b2
    kx, ky = k_C[0], k_C[1]
    J1, J2, J3, Az = J_list[0], J_list[1], J_list[2], J_list[3]
    z1, z2, z3 = z_list
    Ak = Az + z1 * J1 + z2 * J2 + z3 * J3 - J1 * 2 * (np.cos(kx) + 2*np.cos(kx/2)*np.cos(np.sqrt(3)*ky/2)) \
    - J2 * 2 * (np.cos(np.sqrt(3)*ky) + 2*np.cos(3*kx/2)*np.cos(np.sqrt(3)*ky/2)) \
    - J3 * 2 * (np.cos(2*kx) + 2*np.cos(kx)*np.cos(np.sqrt(3)*ky))
    return S * Ak


fig, ax = plt.subplots(figsize=(4,3))
fs = 12  # Font size
lw = 1   # Line width

X, E, d = cri3.band(kpoints, H_k, Nb=1, Nk=51)
ax.plot(X, E, lw=lw, color='red', label='LSWT')

# TDDFT from turbo_magnon QE
xx = np.array([0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]) * d[-1]
e = np.array([8.3, 13.4, 23.6, 44.4, 65.0, 80.4, 91, 103, 103, 97, 78])


ax.scatter(xx, e, label='TDDFT')

# format
ax.set_xlim(d[0],d[-1])
ax.set_ylim(0,)
xlabels = [r'$\Gamma$', 'M']
ax.tick_params(labelsize=fs-2)
ax.set_xticks(d, xlabels)
ax.set_ylabel('Energy (meV)', fontsize=fs)
ax.legend(fontsize=fs-2, facecolor='none', edgecolor='none')

plt.savefig('TDDFT_magnon.pdf', dpi=300, bbox_inches='tight')
plt.savefig('TDDFT_magnon.png', dpi=300, bbox_inches='tight')

plt.show()