import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap, Normalize

# load common data
kpath = 'GKM'
magnon_folder = 'CrI3/data_magnon'
phonon_folder = 'CrI3/data_phonon'
# load eigenvalues and eigenvectors of phonon
phonon_energies = np.load(f'{phonon_folder}/eigenvalues_{kpath}.npy')
print('shape of phonon energies', phonon_energies.shape)
# load q points
q_points = np.load(f'{phonon_folder}/q_points_{kpath}.npy')

# Plotting
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(10, 4))
fs=16

# DFT PBE
label = 'DFT'
# load eigenvalues and eigenvectors of magnon
magnon_energies = np.load(f'{magnon_folder}/eigenvalues_{kpath}_{label}.npy')
print('shape of magnon energies', magnon_energies.shape)
# load coupling constants
lambda_q = np.load(f'./CrI3/supercell_3x3/lambda_{kpath}_{label}.npy')
print('shape of couling constants', lambda_q.shape)
# number of bands
nq = phonon_energies.shape[0]
nb_ph = phonon_energies.shape[1]
nb_mag = magnon_energies.shape[1]
nb = nb_ph + nb_mag

magnon_weight = np.zeros((nq,nb))
eigenvalues = np.zeros((nq,nb))

for iq in range(nq):
    # construct the Hamiltonian
    H_k = np.zeros((nb, nb), dtype=complex)
    # magnon
    for ib in range(nb_mag):
        H_k[ib, ib] = magnon_energies[iq, ib]
    # phonon
    for ib in range(nb_ph):
        H_k[ib+nb_mag,ib+nb_mag] = phonon_energies[iq, ib]
    # coupling
    for i in range(nb_mag):
        for j in range(nb_ph):
            H_k[i,j+nb_mag] = lambda_q[iq,i,j]
            H_k[j+nb_mag,i] = lambda_q[iq,i,j].conj()

    eig, eigv = np.linalg.eig(H_k)
    # calculate magnon weight
    weight = np.linalg.norm(eigv[0:nb_mag,:], axis=0)
    eigenvalues[iq,:] = np.real(eig)
    magnon_weight[iq, :] = weight

print('shape of eigenvalues: ', eigenvalues.shape)
print('shape of magnon weight: ', magnon_weight.shape)
print('magnon weight: ', magnon_weight)

# plotting
index_K = 501
xx = np.arange(1002)
xticks = [0, 501, 1002]
xticklabels = [r'$\Gamma$', 'K', 'M']

# Define custom colormap from blue to red
colors = [(0, 0, 1), (1, 0, 0)]  # Blue to Red
n_bins = 100  # Discretize the interpolation into bins
cmap_name = 'blue_red'
# Create the colormap
cm = LinearSegmentedColormap.from_list(cmap_name, colors, N=n_bins)
norm = Normalize(vmin=0, vmax=1)

for ib in range(nb):
    scatter1 = ax1.scatter(xx, eigenvalues[:,ib], s=0.5, c=magnon_weight[:,ib], cmap=cm, alpha=0.5, norm=norm)

#cb1 = fig.colorbar(scatter1, ax=ax1)
#cb1.ax.tick_params(labelsize=fs-2)

ax1.set_xlim(0, 1002)
ax1.set_ylim(8,16)
ax1.set_ylabel('Energy (meV)', fontsize=fs+2)
ax1.set_xticks(xticks, xticklabels)
ax1.tick_params(labelsize=fs-2)
for xtick in xticks:
    ax1.axvline(x=xtick, color='black', linewidth=0.5)

# DFT LDA
label = 'DFT_LDA'
# load eigenvalues and eigenvectors of magnon
magnon_energies = np.load(f'{magnon_folder}/eigenvalues_{kpath}_{label}.npy')
print('shape of magnon energies', magnon_energies.shape)
# load coupling constants
lambda_q = np.load(f'./CrI3/LDA/lambda_{kpath}_{label}.npy')
print('shape of couling constants', lambda_q.shape)
# number of bands
nq = phonon_energies.shape[0]
nb_ph = phonon_energies.shape[1]
nb_mag = magnon_energies.shape[1]
nb = nb_ph + nb_mag

magnon_weight = np.zeros((nq,nb))
eigenvalues = np.zeros((nq,nb))

for iq in range(nq):
    # construct the Hamiltonian
    H_k = np.zeros((nb, nb), dtype=complex)
    # magnon
    for ib in range(nb_mag):
        H_k[ib, ib] = magnon_energies[iq, ib]
    # phonon
    for ib in range(nb_ph):
        H_k[ib+nb_mag,ib+nb_mag] = phonon_energies[iq, ib]
    # coupling
    for i in range(nb_mag):
        for j in range(nb_ph):
            H_k[i,j+nb_mag] = lambda_q[iq,i,j]
            H_k[j+nb_mag,i] = lambda_q[iq,i,j].conj()

    eig, eigv = np.linalg.eig(H_k)
    # calculate magnon weight
    weight = np.linalg.norm(eigv[0:nb_mag,:], axis=0)
    eigenvalues[iq,:] = np.real(eig)
    magnon_weight[iq, :] = weight

print('shape of eigenvalues: ', eigenvalues.shape)
print('shape of magnon weight: ', magnon_weight.shape)
print('magnon weight: ', magnon_weight)

# plotting
index_K = 501
xx = np.arange(1002)
xticks = [0, 501, 1002]
xticklabels = [r'$\Gamma$', 'K', 'M']

# Define custom colormap from blue to red
colors = [(0, 0, 1), (1, 0, 0)]  # Blue to Red
n_bins = 100  # Discretize the interpolation into bins
cmap_name = 'blue_red'
# Create the colormap
cm = LinearSegmentedColormap.from_list(cmap_name, colors, N=n_bins)
norm = Normalize(vmin=0, vmax=1)

for ib in range(nb):
    scatter2 = ax2.scatter(xx, eigenvalues[:,ib], s=0.5, c=magnon_weight[:,ib], cmap=cm, alpha=0.5, norm=norm)

#cb2 = fig.colorbar(scatter2, ax=ax2)
#cb2.ax.tick_params(labelsize=fs-2)

ax2.set_xlim(0, 1002)
ax2.set_ylim(8,16)
#ax2.set_ylabel('Energy (meV)', fontsize=fs+2)
ax2.set_xticks(xticks, xticklabels)
ax2.tick_params(labelsize=fs-2)
for xtick in xticks:
    ax2.axvline(x=xtick, color='black', linewidth=0.5)

ax1.text(0.02, 0.97, '(a)', transform=ax1.transAxes, fontsize=fs+2, va='top', ha='left')
ax2.text(0.02, 0.97, '(b)', transform=ax2.transAxes, fontsize=fs+2, va='top', ha='left')
# save the figure
plt.savefig(f'combined_magnon_LDA_PBE.png', transparent=True, dpi=300, bbox_inches='tight')
plt.savefig(f'combined_magnon_LDA_PBE.pdf', transparent=True, dpi=300, bbox_inches='tight')
plt.show()