import numpy as np
import matplotlib.pyplot as plt

data_cri3 = np.loadtxt('./CrI3/phonopy/band_no_LO-TO.dat')
data_cri3_loto = np.loadtxt('./CrI3/phonopy/band_LO-TO.dat')
data_crte2 = np.loadtxt('./CrTe2/data_phonon/band.dat')

fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 3))
fs = 14

# unit from THz to meV
unit = 4.136

# CrI3
nph = 24
k1 = data_cri3[:,0].reshape((nph, -1))[0,:]
k2 = data_cri3_loto[:,0].reshape((nph, -1))[0,:]
e1 = data_cri3[:,1].reshape((nph, -1))
e2 = data_cri3_loto[:,1].reshape((nph, -1))

for i in range(nph-1):
    ax1.plot(k1, e1[i,:]*unit, color='blue')
    ax1.plot(k2, e2[i,:]*unit, color='red', ls='--')

ax1.plot(k1, e1[nph-1,:]*unit, color='blue', label='without LOTO splitting')
ax1.plot(k2, e2[nph-1,:]*unit, color='red', ls='--', label='with LOTO splitting')
xticks = [0.00000000, 0.09521360, 0.14282050, 0.22527870]
xlabels = [r'$\Gamma$', 'K', 'M', r'$\Gamma$']
ax1.set_xticks(xticks, xlabels)
ax1.set_xlim(k1[0], k1[-1])
ax1.set_ylim(0,)
ax1.set_ylabel('Energy (meV)', fontsize=fs)
ax1.legend(frameon=False, loc=(0.3,0.5))
ax1.text(-0.19, 0.97, '(a)', transform=ax1.transAxes, fontsize=fs+2, va='top', ha='left')

# CrTe2
nph = 9
k3 = data_crte2[:,0].reshape((nph, -1))[0,:]
e3 = data_crte2[:,1].reshape((nph, -1))
xticks = [0.00000000, 0.17967760, 0.26951630, 0.42512320]
xlabels = [r'$\Gamma$', 'K', 'M', r'$\Gamma$']
ax2.set_xlim(k3[0], k3[-1])
for i in range(nph):
    ax2.plot(k3, e3[i,:]*unit, color='blue')
ax2.set_xticks(xticks, xlabels) 
ax2.set_ylabel('Energy (meV)', fontsize=fs)   
ax2.text(-0.19, 0.97, '(b)', transform=ax2.transAxes, fontsize=fs+2, va='top', ha='left')
ax2.set_ylim(0,)

plt.tight_layout()
plt.savefig('combined_phonon.pdf', dpi=300, bbox_inches='tight')
plt.show()