import numpy as np
np.set_printoptions(suppress=True)

def read_position(poscar):
    """
    @return: lattice vectors, coords, 
    """
    # read poscar
    with open(poscar, 'r') as f:
        lines = f.readlines()

    # read lattice vectors
    la = np.array(lines[2].split(), dtype=float)
    lb = np.array(lines[3].split(), dtype=float)
    lc = np.array(lines[4].split(), dtype=float)

    # read element label and number
    labels = lines[5].split()
    nums = np.array(lines[6].split(), dtype=int)
    total_num = np.sum(nums)

    # read coordinates
    coords = []
    for i in range(total_num):
        coord = np.array(lines[i+8].split(), dtype=float)
        coords.append(coord)
    coords= np.array(coords)
    
    return la, lb, lc, coords 

def get_reciprocal(lattice):
    """
    Calculates the reciprocal lattice vectors.
    Input: lattice a list of [a1, a2, a3]
    Returns:
        tuple of numpy arrays: Reciprocal lattice vectors (b1, b2, b3).
    """
    a1, a2, a3 = lattice[0], lattice[1], lattice[2]
    V = np.dot(a1, np.cross(a2, a3))
    b1 = 2 * np.pi * np.cross(a2, a3) / V
    b2 = 2 * np.pi * np.cross(a3, a1) / V
    b3 = 2 * np.pi * np.cross(a1, a2) / V
    return b1, b2, b3