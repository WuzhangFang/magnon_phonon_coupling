import numpy as np
import matplotlib.pyplot as plt
from functions import get_reciprocal, read_position

# lattice vectors
a1, a2, a3, _ = read_position('POSCAR.vasp')
# reciprocal lattice vectors in Cartesian
b1, b2, b3 = get_reciprocal([a1, a2, a3])

# load q points in Direct
qpoints = np.load('phonopy/q_points_q3x3.npy')
#qpoints = np.load('phonopy/q_points_q31x31.npy')
print('shape of qpoints: ', qpoints.shape)

# convert to Cartesian
qx = [q1 * b1[0] + q2 * b2[0] for q1, q2, _ in qpoints]
qy = [q1 * b1[1] + q2 * b2[1] for q1, q2, _ in qpoints]

# plot
plt.figure(figsize=(6,6))
plt.scatter(qx, qy)
plt.show()